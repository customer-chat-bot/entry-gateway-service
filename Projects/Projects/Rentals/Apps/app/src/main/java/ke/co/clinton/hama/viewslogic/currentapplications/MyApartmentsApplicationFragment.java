package ke.co.clinton.hama.viewslogic.currentapplications;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.entities.House;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyApartmentsApplicationFragment extends Fragment {

    private RecyclerView rvSavedApartments;

    private CurrentApplicationsAdapter currentApplicationsAdapter;

    private List<House> savedApartmentsList;


    public MyApartmentsApplicationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_saved_apartments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvSavedApartments = view.findViewById(R.id.rv_saved_apartments);

        setUpSavedApartments();

        swipeToDelete();
    }

    private void setUpSavedApartments(){

        currentApplicationsAdapter = new CurrentApplicationsAdapter(getActivity());
        rvSavedApartments.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSavedApartments.setAdapter(currentApplicationsAdapter);
    }

    private void swipeToDelete () {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {

                    final House removedApartment = savedApartmentsList.get(position);
                    final int deletedPosition = position;
                    currentApplicationsAdapter.removeSavedApartment(position);

                    Snackbar snackbar = Snackbar.make(getActivity().getWindow().getDecorView().getRootView(),
                            "Saved apartment removed", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            currentApplicationsAdapter.restoreSavedApartment(removedApartment, deletedPosition);
                        }
                    });
                    snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                    snackbar.show();

                }else {
                    final House removedApartment = savedApartmentsList.get(position);
                    final int deletedPosition = position;
                    currentApplicationsAdapter.removeSavedApartment(position);

                    Snackbar snackbar = Snackbar.make(getActivity().getWindow().getDecorView().getRootView(),
                            "Saved apartment removed", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            currentApplicationsAdapter.restoreSavedApartment(removedApartment,deletedPosition);
                        }
                    });
                    snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                    snackbar.show();
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvSavedApartments);
    }
}
