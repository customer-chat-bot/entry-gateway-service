package ke.co.clinton.hama.viewslogic.currentapplications;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SavedApartmentsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SavedApartmentsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is saved apartments");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
