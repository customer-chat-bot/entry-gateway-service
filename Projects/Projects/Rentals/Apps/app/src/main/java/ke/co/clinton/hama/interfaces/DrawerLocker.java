package ke.co.clinton.hama.interfaces;

public interface DrawerLocker {
    void setDrawerEnabled(boolean enabled);
}
