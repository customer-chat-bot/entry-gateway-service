package ke.co.clinton.hama.viewslogic.savedhouses;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import ke.co.clinton.hama.dataLayer.entities.House;
import ke.co.clinton.hama.dataLayer.repository.HouseRepository;

public class SavedHousesViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;

    private HouseRepository houseRepository;
    private LiveData<List<House>> allSavedHouses;

    public SavedHousesViewModel(@NonNull Application application) {
        super(application);

        houseRepository = new HouseRepository(application);
        allSavedHouses = houseRepository.getAllSavedHouses();
    }

    public void insert(House house) {
        houseRepository.insert(house);
    }

    public void update(House house) {
        houseRepository.update(house);
    }

    public void delete(House house) {
        houseRepository.delete(house);
    }

    public LiveData<List<House>> getAllSavedHouses(){
        return allSavedHouses;
    }

    public void deleteAllHouse() {
        houseRepository.deleteAllHouses();
    }

    public void deleteAllSavedHouses() {
        houseRepository.deleteAllSavedHouses();
    }



//    public SavedHousesViewModel() {
//        mText = new MutableLiveData<>();
//        mText.setValue("This is etjjtyghj bids");
//    }



    /*public LiveData<String> getText() {
        return mText;
    }*/
}