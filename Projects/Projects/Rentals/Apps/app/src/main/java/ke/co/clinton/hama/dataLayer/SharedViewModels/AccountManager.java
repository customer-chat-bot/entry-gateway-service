package ke.co.clinton.hama.dataLayer.SharedViewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ke.co.clinton.hama.dataLayer.entities.User;

public class AccountManager extends AndroidViewModel {
    private static final String TAG = "AccountManager";

    private MutableLiveData<User> userMutableLiveData =  new MutableLiveData<>();

    public AccountManager(@NonNull Application application) {
        super(application);
    }

    public LiveData<User> getUserDetails(){
        return userMutableLiveData;
    }

    public void setUserDetails(User user) {
        Log.d(TAG, "setUserDetails: Callded now : "+user);
        userMutableLiveData.postValue(user);
//        if(userMutableLiveData == null){
//            userMutableLiveData.setValue(user);
//        }
    }

}
