package ke.co.clinton.hama.dataLayer.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import org.json.JSONObject;

@Entity(tableName = "house_table")
public class House {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String id;
    private String houseName;
    private String descriptiom;
    private String location;
    private String bedrooms;
    private String rent;
    private String imageUrl;
    private String isSaved;
    private String isApplied;

    public House() {
    }

    @Ignore
    public House(String hId, String houseName, String descriptiom, String location, String bedrooms, String rent, String imageUrl,
                 String isSaved,String isApplied) {
        this.id = hId;
        this.houseName = houseName;
        this.descriptiom = descriptiom;
        this.location = location;
        this.bedrooms = bedrooms;
        this.rent = rent;
        this.imageUrl = imageUrl;
        this.isSaved = isSaved;
        this.isApplied = isApplied;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getDescriptiom() {
        return descriptiom;
    }

    public void setDescriptiom(String descriptiom) {
        this.descriptiom = descriptiom;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRent() {
        return "KES "+rent+"/mo";
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getIsSaved() {
        return isSaved;
    }

    public void setIsSaved(String isSaved) {
        this.isSaved = isSaved;
    }

    public String getIsApplied() {
        return isApplied;
    }

    public void setIsApplied(String isApplied) {
        this.isApplied = isApplied;
    }

    JSONObject asJSON() {
        JSONObject data= new JSONObject();
        try{
            data.put("id", id);
            data.put("houseName", houseName);
            data.put("descriptiom", descriptiom);
            data.put("location", location);
            data.put("bedrooms", bedrooms);
            data.put("rent", rent);
            data.put("imageUrl", imageUrl);
            data.put("isSaved", isSaved);
            data.put("isApplied", isApplied);

        }catch (Exception e){}

        return data;
    }

    @Override
    public String toString() {
        return asJSON().toString();
    }
}
