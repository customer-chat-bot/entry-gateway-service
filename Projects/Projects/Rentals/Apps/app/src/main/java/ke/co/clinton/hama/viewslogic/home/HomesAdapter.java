package ke.co.clinton.hama.viewslogic.home;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.entities.House;

public class HomesAdapter extends RecyclerView.Adapter<HomesAdapter.MyViewHolder>  {

    private List<House> mDataset;

    private List<House> mDatasetSearch;

    private  OnHouseClickedListener onHouseClickedListener;

    private static final String TAG = "HomesAdapter";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder


    public HomesAdapter(OnHouseClickedListener onHouseClickedListener) {
        mDataset = new ArrayList();
        this.onHouseClickedListener = onHouseClickedListener;
        //This is for Search
        mDatasetSearch = new ArrayList();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HomesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View root =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_house_display, parent, false);
        return new MyViewHolder(root, onHouseClickedListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        House house = mDataset.get(position);

        TextView rent = holder.view.findViewById(R.id.rent);
        rent.setText(house.getRent());

        TextView location = holder.view.findViewById(R.id.location);
        location.setText(house.getLocation());

        TextView bedrooms = holder.view.findViewById(R.id.bedrooms);
        bedrooms.setText(house.getBedrooms()+" bedroom(s)");

        ImageView imageView = holder.view.findViewById(R.id.houseImage);
        String imageName= house.getImageUrl();

        if(imageName.equals("1")){
            imageView.setImageResource(R.drawable.house1);
        } else if(imageName.equals("2")){
            imageView.setImageResource(R.drawable.house2);
        } else {
            imageView.setImageResource(R.drawable.house3);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setHousesRefresh(List<House> houseList){
        Log.d(TAG, "setHousesRefresh: Called from above");
        this.mDataset = houseList;
        this.mDatasetSearch = houseList;
        notifyDataSetChanged();
    }

    public void setSearchResult(String text){
        Log.d(TAG, "setHousesRefresh: Called from above");
        if(this.mDatasetSearch.isEmpty()){
            this.mDatasetSearch = this.mDataset;
        }
        List<House> filteredList = new ArrayList<>();
        String filterPattern = text.toLowerCase().trim();
        //Product product is the name of your Items class
        for (House house : mDatasetSearch) {
            //Houses to be searched here
            //Here we search using HouseName
            if (house.getHouseName().toLowerCase().contains(filterPattern)) {
                filteredList.add(house);
            }
            // Here we search using Bedrooms
            else  if (house.getBedrooms().toLowerCase().contains(filterPattern)){
                filteredList.add(house);
            }
            // Here we search using Location
            else if (house.getLocation().toLowerCase().contains(filterPattern)){
                filteredList.add(house);
            }
            // Here we search using Rent
            else if (house.getRent().toLowerCase().contains(filterPattern)){
                filteredList.add(house);
            }
        }

        this.mDataset = filteredList;
        notifyDataSetChanged();
    }

    public void resetList(){
        if(!this.mDatasetSearch.isEmpty()){
            this.mDataset = this.mDatasetSearch;
            notifyDataSetChanged();
        }
    }

    public House getHouseClicked(int position){
        return mDataset.get(position);
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public View view;
        private OnHouseClickedListener onHouseClickedListener;

        public MyViewHolder(View v, OnHouseClickedListener onHouseClickedListener) {
            super(v);
            view = v;
            this.onHouseClickedListener = onHouseClickedListener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onHouseClickedListener.OnHouseClick(getAdapterPosition());
        }
    }


    public interface OnHouseClickedListener{
        void OnHouseClick(int position);
    }

    public Filter getHouseFilter() {
        return houseFilter;
    }

    private Filter houseFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<House> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mDatasetSearch);
            }else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                //Product product is the name of your Items class
                for (House house : mDatasetSearch) {
                    //Houses to be searched here
                    //Here we search using HouseName
                    if (house.getHouseName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(house);
                    }

                    // Here we search using Bedrooms
                    if (house.getBedrooms().toLowerCase().contains(filterPattern)){
                        filteredList.add(house);
                    }

                    // Here we search using Location
                    if (house.getLocation().toLowerCase().contains(filterPattern)){
                        filteredList.add(house);
                    }

                    // Here we search using Rent
                    if (house.getRent().toLowerCase().contains(filterPattern)){
                        filteredList.add(house);
                    }

                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            mDataset.clear();
            mDataset.addAll((List)filterResults.values);
            notifyDataSetChanged();
        }
    };
}