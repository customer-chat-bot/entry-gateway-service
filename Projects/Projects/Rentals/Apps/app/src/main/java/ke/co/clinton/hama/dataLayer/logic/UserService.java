package ke.co.clinton.hama.dataLayer.logic;

import android.content.Context;

import androidx.room.Room;

import java.util.List;

import ke.co.clinton.hama.dataLayer.config.AppConfigAO;
import ke.co.clinton.hama.dataLayer.dao.UserDao;
import ke.co.clinton.hama.dataLayer.entities.User;

public class UserService {

    private AppConfigAO appConfigAO;

    private UserDao userDao;

    public UserService(Context context) {

        if(appConfigAO == null){
            appConfigAO = Room.databaseBuilder(context, AppConfigAO.class, "mi-database.db")
                    .allowMainThreadQueries()
                    .build();
        }
    }

    public User getUserDetails(String email, String password){
        userDao = appConfigAO.getUserDao();
        return userDao.getUser(email, password);
    }

    public void addUserDetails(User user){
        userDao = appConfigAO.getUserDao();
        userDao.insert(user);
    }

    public List<User> getUsers(){
        userDao = appConfigAO.getUserDao();
        List<User> userList = userDao.getAll();

        return userList;
    }


}
