package ke.co.clinton.hama.viewslogic.currentapplications;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.entities.House;

public class CurrentApplicationsAdapter extends RecyclerView.Adapter<CurrentApplicationsAdapter.MyViewHolder> {

    private Context context;
    private List<House> savedApartmentsList = new ArrayList<>();

    public CurrentApplicationsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_house_display,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return savedApartmentsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.rent.setText(savedApartmentsList.get(position).getRent());
        holder.location.setText(savedApartmentsList.get(position).getLocation());
        holder.bedrooms.setText(savedApartmentsList.get(position).getBedrooms()+" bedroom(s)");

        holder.imageView.setImageResource(R.drawable.house1);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView rent, location, bedrooms;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            rent = itemView.findViewById(R.id.rent);
            location = itemView.findViewById(R.id.location);
            bedrooms = itemView.findViewById(R.id.bedrooms);
            imageView = itemView.findViewById(R.id.houseImage);
        }

    }

    public void removeSavedApartment(int position) {
        savedApartmentsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, savedApartmentsList.size());
    }

    public void restoreSavedApartment(House house, int position) {
        savedApartmentsList.add(position, house);
        // notify item added by position
        notifyItemInserted(position);
    }
}
