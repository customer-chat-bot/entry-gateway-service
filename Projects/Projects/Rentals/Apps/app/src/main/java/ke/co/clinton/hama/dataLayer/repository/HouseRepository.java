package ke.co.clinton.hama.dataLayer.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.clinton.hama.dataLayer.dao.HouseDao;
import ke.co.clinton.hama.dataLayer.database.HouseDatabase;
import ke.co.clinton.hama.dataLayer.entities.House;

public class HouseRepository {

    private HouseDao houseDao;

    private LiveData<List<House>> allHouses;

    private LiveData<List<House>> allSavedHouses;

    private LiveData<List<House>> allAppliedHouses;

    public HouseRepository(Application application) {
        HouseDatabase houseDatabase = HouseDatabase.getInstance(application);
        houseDao = houseDatabase.houseDao();

        allHouses = houseDao.getAllHousesLiveData();
        allSavedHouses = houseDao.getAllSavedHouses();
        allAppliedHouses = houseDao.getAllAppliedHouses();
    }

    public void insert(House house) {
        new InsertHouseAsyncTask(houseDao).execute(house);
    }

    public void update(House house) {
        new UpdateHouseAsyncTask(houseDao).execute(house);
    }

    public LiveData<List<House>> getAllHouses() {
        return allHouses;
    }

    public LiveData<List<House>> getAllSavedHouses() {
        return allSavedHouses;
    }

    public LiveData<List<House>> getAllAppliedHouses() {
        return allAppliedHouses;
    }

    public void delete(House house) {
        new DeleteHouseAsyncTask(houseDao).execute(house);
    }

    public void deleteAllHouses() {
        new DeleteAllHousesAsyncTask(houseDao).execute();
    }


    public void deleteAllSavedHouses() {
        new DeleteAllSavedHousesAsyncTask(houseDao).execute();
    }

    public void deleteAllAppliedHouses() {
        new DeleteAllAppliedHousesAsyncTask(houseDao).execute();
    }

    private static class InsertHouseAsyncTask extends AsyncTask<House, Void, Void> {

        private HouseDao houseDao;

        public InsertHouseAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(House... houses) {
            houseDao.insert(houses[0]);
            return null;
        }
    }

    private static class UpdateHouseAsyncTask extends AsyncTask<House, Void, Void> {

        private HouseDao houseDao;

        public UpdateHouseAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(House... houses) {
            houseDao.update(houses[0]);
            return null;
        }
    }

    private static class DeleteHouseAsyncTask extends AsyncTask<House, Void, Void> {

        private HouseDao houseDao;

        public DeleteHouseAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(House... houses) {
            houseDao.delete(houses[0]);
            return null;
        }
    }


    private static class DeleteAllHousesAsyncTask extends AsyncTask<Void, Void, Void> {

        private HouseDao houseDao;

        public DeleteAllHousesAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            houseDao.deleteAllHouses();
            return null;
        }
    }

    private static class DeleteAllSavedHousesAsyncTask extends AsyncTask<Void, Void, Void> {

        private HouseDao houseDao;

        public DeleteAllSavedHousesAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            houseDao.deleteAllSavedHouses();
            return null;
        }
    }

    private static class DeleteAllAppliedHousesAsyncTask extends AsyncTask<Void, Void, Void> {

        private HouseDao houseDao;

        public DeleteAllAppliedHousesAsyncTask(HouseDao houseDao) {
            this.houseDao = houseDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            houseDao.deleteAllAppliedHouses();
            return null;
        }
    }

}
