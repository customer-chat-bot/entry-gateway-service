package ke.co.clinton.hama.dataLayer.logic;

import org.json.JSONObject;

import ke.co.clinton.hama.dataLayer.entities.House;

public class HouseService {

    public House getHouseFromBundle(String input){
        House house = null;
        try{
            JSONObject data= new JSONObject(input);
            String hIdi = data.has("id") ? data.getString("id") : null;
            String houseNamei = data.has("houseName") ? data.getString("houseName") : null;
            String descriptiomi = data.has("descriptiom") ? data.getString("descriptiom") : null;
            String locationi = data.has("location") ? data.getString("location") : null;
            String bedroomsi = data.has("bedrooms") ? data.getString("bedrooms") : null;
            String renti = data.has("rent") ? data.getString("rent") : null;
            String imageUrl = data.has("imageUrl") ? data.getString("imageUrl") : null;

            String isSaved = data.has("isSaved") ? data.getString("isSaved") : null;
            String isApplied = data.has("isApplied") ? data.getString("isApplied") : null;

            house = new House( hIdi, houseNamei, descriptiomi, locationi, bedroomsi, renti, imageUrl, isSaved, isApplied);
        }catch (Exception e){}
        return house;
    }
}
