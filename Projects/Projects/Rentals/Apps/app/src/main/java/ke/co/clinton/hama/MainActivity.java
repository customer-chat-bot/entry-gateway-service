package ke.co.clinton.hama;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import ke.co.clinton.hama.dataLayer.SharedViewModels.AccountManager;
import ke.co.clinton.hama.dataLayer.entities.User;
import ke.co.clinton.hama.interfaces.AddRemoveCallbacks;
import ke.co.clinton.hama.interfaces.DrawerLocker;

public class MainActivity extends AppCompatActivity implements DrawerLocker, NavigationView.OnNavigationItemSelectedListener,
        FragmentManager.OnBackStackChangedListener, AddRemoveCallbacks {

    private static final String TAG = "MainActivity";

    public static int house_counter = 2;


    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;

    private AccountManager accountManager;
    private NavigationView navigationView;
    private NavController navController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_saved_houses, R.id.nav_my_applications, R.id.nav_my_houses,
                R.id.nav_payment_history, R.id.nav_account_management, R.id.nav_login_logout)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.setGraph(R.navigation.mobile_navigation);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.bringToFront();

        // Setting NavigationView Menu item saved houses with counter on the right
        // Make sure to set app:actionLayout="" to the menu item in findItem
        LinearLayout houseCounterPanel = (LinearLayout) navigationView.getMenu().findItem(R.id.nav_saved_houses).getActionView();
        TextView tvHouseCounter = houseCounterPanel.findViewById(R.id.tv_house_counter);

        if (house_counter > 0) {
            houseCounterPanel.setVisibility(View.VISIBLE);
            tvHouseCounter.setText(house_counter > 0 ? String.valueOf(house_counter) : null);
        }else {
            houseCounterPanel.setVisibility(View.GONE);
        }


        accountManager = ViewModelProviders.of(this).get(AccountManager.class);
        accountManager.getUserDetails().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                Log.d(TAG, "onChanged:-------------------------|||||-- "+ user.toString());
                changeNavidationUI();
            }
        });
    }

    private void changeNavidationUI(){
        Log.d(TAG, "changeNavidationUI: Changing Nav =++++++++++++++++++++++++++");
        if(navigationView == null){
            navigationView = findViewById(R.id.nav_view);
        }
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_saved_houses, R.id.nav_my_applications, R.id.nav_my_houses,
                R.id.nav_payment_history)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.setGraph(R.navigation.mobile_navigation_logged_in);

        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.bringToFront();


    }

    public static void setHouseCounter(@IdRes int houseId, int count, NavigationView navigationView) {
        TextView tvHouseCounter = (TextView) navigationView.getMenu().findItem(1).getActionView();
        tvHouseCounter.setText(count > 0 ? String.valueOf(count) : null);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }


    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawer.setDrawerLockMode(lockMode);
    }

    @Override
    public void onBackStackChanged() {
        Log.d(TAG, "onBackStackChanged: Total fragments here : ==========++++++++++++++++++++++++============ ");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onSaveHouse() {

    }

    @Override
    public void onRemoveHouse() {

    }
}
