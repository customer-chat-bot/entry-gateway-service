package ke.co.clinton.hama.viewslogic.account;

import android.app.ProgressDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputLayout;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.SharedViewModels.AccountManagementViewModel;
import ke.co.clinton.hama.dataLayer.entities.User;
import ke.co.clinton.hama.dataLayer.logic.UserService;

public class AccountManagementFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = "AccountManagementFragme";

    private AccountManagementViewModel mViewModel;

    private TextInputLayout tilEmail, tilPassword;

    private Button btnSignIn;

    private TextView tvSignUp;

    private EditText etEmail, etPassword;

    private ProgressDialog progressDialog;


    private UserService userServiceo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        userServiceo = new UserService(getActivity());
        return inflater.inflate(R.layout.fragment_account_management, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AccountManagementViewModel.class);
        // TODO: Use the ViewModel

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Checking User....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        //progressDialog.show();

        btnSignIn = view.findViewById(R.id.btn_sign_in);
        tvSignUp = view.findViewById(R.id.tv_sign_up);

        etEmail = view.findViewById(R.id.et_email);
        etPassword = view.findViewById(R.id.et_password);

        btnSignIn.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

        // Underline TextView
        tvSignUp.setPaintFlags(tvSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:
                signIn();
                break;
            case R.id.tv_sign_up:
                signUp();
                break;
                default:
                    break;
        }
    }

    private void signIn() {
        if (!emptyDetailsValidation()) {
            progressDialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    User user = userServiceo.getUserDetails(etEmail.getText().toString(), etPassword.getText().toString());
                    if(user!=null) {
                        Toast.makeText(getActivity(), "Sign in successful", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getActivity(), "User doesn't exist sign up", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            },1000);
        }else {
            Toast.makeText(getActivity(), "Email and password has not been entered", Toast.LENGTH_SHORT).show();
        }
    }

    private void signUp() {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new
//                SignUpFragment(), SignUpFragment.class.getSimpleName()).commit();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.animator.enter_anim, R.animator.exit_anim);
        transaction.replace(R.id.nav_host_fragment, new SignUpFragment());
//        transaction.replace(R.id.nav_host_fragment, new AccountManagementFragment());
        transaction.addToBackStack(this.getClass().getName());
        transaction.commit();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: Called now");
    }

    private boolean emptyDetailsValidation () {
        if (TextUtils.isEmpty(etEmail.getText().toString()) || TextUtils.isEmpty(etPassword.getText().toString())) {
            return true;
        }else {
            return false;
            }
    }
}


