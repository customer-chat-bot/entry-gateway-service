package ke.co.clinton.hama.dataLayer.config;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import ke.co.clinton.hama.dataLayer.dao.HouseDao;
import ke.co.clinton.hama.dataLayer.dao.UserDao;
import ke.co.clinton.hama.dataLayer.entities.House;
import ke.co.clinton.hama.dataLayer.entities.User;

@Database(entities = {House.class, User.class}, version = 2, exportSchema = false)

public abstract class AppConfigAO extends RoomDatabase {

    public abstract UserDao getUserDao();
    public abstract HouseDao getHouseDao();
}
