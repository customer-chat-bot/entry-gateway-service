package ke.co.clinton.hama.interfaces;

public interface AddRemoveCallbacks {

    public void onSaveHouse();
    public void onRemoveHouse();

}
