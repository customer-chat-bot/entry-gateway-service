package ke.co.clinton.hama.viewslogic.account;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import ke.co.clinton.hama.MainActivity;
import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.SharedViewModels.AccountManager;
import ke.co.clinton.hama.dataLayer.entities.User;
import ke.co.clinton.hama.dataLayer.logic.UserService;
import ke.co.clinton.hama.interfaces.DrawerLocker;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    private Button btnSignUp;

    private EditText etUsername, etEmail, etPassword;

    private ProgressDialog progressDialog;

    private UserService userServiceo;


    private AccountManager accountManager;

    private static final String TAG = "SignUpFragment";


    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity)getActivity()).getSupportActionBar().hide();
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
        userServiceo = new UserService(getActivity());
        getActivity().invalidateOptionsMenu();
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        Toolbar tbSignUp = view.findViewById(R.id.tb_sign_up);
        tbSignUp.setTitle("Sign Up");
        tbSignUp.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        tbSignUp.setNavigationIcon(R.drawable.ic_back_button);
        tbSignUp.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Registering User...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        btnSignUp = view.findViewById(R.id.btn_sign_up_two);

        etUsername = view.findViewById(R.id.et_sign_up_username);
        etEmail = view.findViewById(R.id.et_sign_up_email);
        etPassword = view.findViewById(R.id.et_sign_up_password);

        accountManager = ViewModelProviders.of(getActivity()).get(AccountManager.class);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!emptyDetailsValidation()) {

                    progressDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            User user = new User( etUsername.getText().toString(), etEmail.getText().toString(),
                                    etPassword.getText().toString());
                            accountManager.setUserDetails(user);

//                            userServiceo.addUserDetails(user);
//
//                            List<User> userList = userServiceo.getUsers();
//                            for(User user1: userList){
//                                Log.d(TAG, "run: "+ user1.toString());
//                            }
                            progressDialog.dismiss();
//                            getActivity().onBackPressed();
                        }
                    }, 1000);
                }else {
                    Toast.makeText(getActivity(), "All the activities have not been entered", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).getSupportActionBar().show();
        ((DrawerLocker) getActivity()).setDrawerEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getSupportActionBar().hide();
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
    }

    private boolean emptyDetailsValidation() {
        if (TextUtils.isEmpty(etUsername.getText().toString()) ||
                TextUtils.isEmpty(etEmail.getText().toString()) ||
                TextUtils.isEmpty(etPassword.getText().toString())
        ) {
            return true;
        } else {
            return false;
        }
    }
}
