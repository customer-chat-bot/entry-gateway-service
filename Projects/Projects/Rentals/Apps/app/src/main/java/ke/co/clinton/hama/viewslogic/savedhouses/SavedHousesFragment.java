package ke.co.clinton.hama.viewslogic.savedhouses;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.SharedViewModels.HousesViewModel;
import ke.co.clinton.hama.dataLayer.entities.House;

public class SavedHousesFragment extends Fragment {

    private SavedHousesAdapter savedHousesAdapter;

    private RecyclerView rvSavedHouses;

    private HousesViewModel housesViewModel;

    private static final String TAG = "SavedHousesFragment";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_saved_houses, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvSavedHouses = view.findViewById(R.id.rv_saved_houses);

        rvSavedHouses.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSavedHouses.setHasFixedSize(true);

        savedHousesAdapter = new SavedHousesAdapter();
        rvSavedHouses.setAdapter(savedHousesAdapter);

        housesViewModel = new ViewModelProvider(this).get(HousesViewModel.class);

        housesViewModel.getSavedHouses().observe(this, new Observer<List<House>>() {
            @Override
            public void onChanged(List<House> houses) {
                savedHousesAdapter.setSavedHousesList(houses);
            }
        });
        rvSavedHouses.setAdapter(savedHousesAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}