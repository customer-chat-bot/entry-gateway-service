package ke.co.clinton.hama.viewslogic.savedhouses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.entities.House;

public class SavedHousesAdapter extends RecyclerView.Adapter<SavedHousesAdapter.MyViewHolder>{

    private List<House> savedHousesList = new ArrayList<>();


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_house_display,parent,false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        House currentSavedHouse = savedHousesList.get(position);


        holder.rent.setText(currentSavedHouse.getRent());
        holder.location.setText(currentSavedHouse.getLocation());
        holder.bedrooms.setText(currentSavedHouse.getBedrooms()+" bedroom(s)");

        holder.imageView.setImageResource(R.drawable.house1);
    }

    @Override
    public int getItemCount() {
        return savedHousesList.size();
    }

    public void setSavedHousesList(List<House> savedHouses){
        this.savedHousesList = savedHouses;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView rent, location, bedrooms;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            rent = itemView.findViewById(R.id.rent);
            location = itemView.findViewById(R.id.location);
            bedrooms = itemView.findViewById(R.id.bedrooms);
            imageView = itemView.findViewById(R.id.houseImage);
        }

    }

}
