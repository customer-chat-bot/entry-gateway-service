package ke.co.clinton.hama.dataLayer.SharedViewModels;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.entities.House;

public class HousesViewModel extends AndroidViewModel {



    private MutableLiveData<List<House>> listMutableLiveDataSaved;
    private MutableLiveData<List<House>> listMutableLiveDataTrending;
    private MutableLiveData<List<House>> listMutableLiveData;

    public HousesViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<House>> getHouses(){
        if (listMutableLiveData == null) {
            listMutableLiveData = new MutableLiveData<>();
            loadHouses();
//            postData();
        }
        return listMutableLiveData;
    }

    public LiveData<List<House>> getSavedHouses() {
        if (listMutableLiveDataSaved == null) {
            listMutableLiveDataSaved = new MutableLiveData<>();
            List<House> myDataset = new ArrayList<>();
            myDataset.add(new House("1", "E26", HousesViewModel.this.getApplication().getString(R.string.desc), "Athi River", "2", "18,500", "1","1","0"));
            myDataset.add(new House("2", "Tamarind Tree Suites Rosslyn by The Tamarind Group ", HousesViewModel.this.getApplication().getString(R.string.desc2), "Tana River", "2", "12500", "rytkj","0","1"));
            listMutableLiveDataSaved.setValue(myDataset);
        }
        return listMutableLiveDataSaved;
    }

    public void setSavedHouse(House house){
        if (listMutableLiveDataSaved == null) {
            listMutableLiveDataSaved = new MutableLiveData<>();
            List<House> myDataset = new ArrayList<>();
            myDataset.add(house);
            listMutableLiveDataSaved.setValue(myDataset);
        } else {
            List<House> myDataset = listMutableLiveDataSaved.getValue();
            myDataset.add(house);
            listMutableLiveDataSaved.postValue(myDataset);
        }
    }



    private void loadHouses() {
        // do async operation to fetch users
        List<House> myDataset = new ArrayList<>();
        myDataset.add(new House("1", "E26", HousesViewModel.this.getApplication().getString(R.string.desc), "Athi River", "2", "18,500", "1","1","0"));
        myDataset.add(new House("2", "Tamarind Tree Suites Rosslyn by The Tamarind Group ", HousesViewModel.this.getApplication().getString(R.string.desc2), "Tana River", "2", "12500", "rytkj","0","1"));
        myDataset.add(new House("3", "Kinoo 1", HousesViewModel.this.getApplication().getString(R.string.desc3), "Westlands near Nairobi", "5", "12500", "3","1","0"));
        myDataset.add(new House("4", "E26", HousesViewModel.this.getApplication().getString(R.string.desc3), "Roysambu", "3", "22,500", "1","0","0"));
        myDataset.add(new House("5", "Eken 54", HousesViewModel.this.getApplication().getString(R.string.desc3), "Westlands Inn  River", "1", "30,500", "2","1","0"));
//        myDataset.add(new House("6", "Farren Hqiu", "yhjd uajbdn iuakjnd", "Parklands Inn River", "1", "12,000", "1","1","0"));
//        myDataset.add(new House("7", "D23HJ", HousesViewModel.this.getApplication().getString(R.string.desc), "RoySambu River", "3", "8,500", "3","0","0"));
//        myDataset.add(new House("8", "JK9012", HousesViewModel.this.getApplication().getString(R.string.desc3), "Thika River", "1", "10,500", "1","1","0"));
//        myDataset.add(new House("9", "Apart 3", "yhjd uajbdn iuakjnd", "Athi Syokimau Near Mlolongo", "2", "52,500", "1","1","0"));
//        myDataset.add(new House("10", "Everlast 001", HousesViewModel.this.getApplication().getString(R.string.desc), "Athi River", "1", "40,500", "2","1","0"));
        listMutableLiveData.setValue(myDataset);
    }

    private void postData() {
        // do async operation to fetch users
        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                List<House> myDataset = new ArrayList<>();
                myDataset.add(new House("1", "E2TTtt6", HousesViewModel.this.getApplication().getString(R.string.desc), "Athi River", "2", "18,500", "1","1","0"));
                myDataset.add(new House("2", "TTTTamarind Tree Suites Rosslyn by The Tamarind Group ", HousesViewModel.this.getApplication().getString(R.string.desc2), "Tana River", "2", "12500", "rytkj","1","0"));
                myDataset.add(new House("3", "KinTTToo 1", HousesViewModel.this.getApplication().getString(R.string.desc3), "Westlands near Nairobi", "5", "12500", "3","0","0"));
                myDataset.add(new House("4", "E26TT", "yhjd uajbdn iuakjnd", "Athi River", "2", "22,500", "rytkj","0","0"));
                myDataset.add(new House("5", "EkenTT 54", HousesViewModel.this.getApplication().getString(R.string.desc3), "Westlands Inn  River", "1", "30,500", "2","1","0"));
                myDataset.add(new House("6", "FarrTen Hqiu", "yhjd uajbdn iuakjnd", "Parklands Inn River", "1", "12,000", "1","1","0"));
                myDataset.add(new House("7", "D23HTTTJ", HousesViewModel.this.getApplication().getString(R.string.desc), "RoySambu River", "3", "8,500", "3","1","0"));
                myDataset.add(new House("8", "JK90TT12", HousesViewModel.this.getApplication().getString(R.string.desc3), "Thika River", "1", "10,500", "1","0","0"));
                myDataset.add(new House("9", "ATTpart 3", "yhjd uajbdn iuakjnd", "Athi Syokimau Near Mlolongo", "2", "52,500", "1","0","1"));
                myDataset.add(new House("10", "ETTTTverlast 001", HousesViewModel.this.getApplication().getString(R.string.desc), "Athi River", "1", "40,500", "2","0","1"));
                listMutableLiveData.postValue(myDataset);
            }
        }, 5000);

    }
}