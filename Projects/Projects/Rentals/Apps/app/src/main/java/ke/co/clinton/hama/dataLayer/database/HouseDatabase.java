package ke.co.clinton.hama.dataLayer.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


import ke.co.clinton.hama.dataLayer.dao.HouseDao;
import ke.co.clinton.hama.dataLayer.entities.House;

@Database(entities = {House.class}, version = 1, exportSchema = false)
public abstract class HouseDatabase extends RoomDatabase {

    private static HouseDatabase houseDatabaseInstance;

    public abstract HouseDao houseDao();

    public static synchronized HouseDatabase getInstance(Context context) {
        if (houseDatabaseInstance == null) {
            houseDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                    HouseDatabase.class, "house_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build();
        }

        return houseDatabaseInstance;
    }

    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateSavedHousesAsyncTask(houseDatabaseInstance).execute();
            new PopulateAppliedHousesAsyncTask(houseDatabaseInstance).execute();
        }
    };

    private static class PopulateSavedHousesAsyncTask extends AsyncTask<Void, Void, Void> {

        private HouseDao houseDao;

        public PopulateSavedHousesAsyncTask(HouseDatabase houseDatabase) {
            houseDao = houseDatabase.houseDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            houseDao.insert(new House("1","Ruaraka flats","Along ruaraka", "Ruaraka",
                    "3 bedrooms","19000", "1","1","1"));
            return null;
        }
    }

    private static class PopulateAppliedHousesAsyncTask extends AsyncTask<Void, Void, Void> {

        private HouseDao houseDao;

        public PopulateAppliedHousesAsyncTask(HouseDatabase houseDatabase) {
            houseDao = houseDatabase.houseDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }


}
