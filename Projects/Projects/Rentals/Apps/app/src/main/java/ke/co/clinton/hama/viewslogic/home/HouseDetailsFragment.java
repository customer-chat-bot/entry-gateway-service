package ke.co.clinton.hama.viewslogic.home;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ke.co.clinton.hama.MainActivity;
import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.SharedViewModels.HousesViewModel;
import ke.co.clinton.hama.dataLayer.logic.HouseService;
import ke.co.clinton.hama.dataLayer.entities.House;
import ke.co.clinton.hama.interfaces.AddRemoveCallbacks;
import ke.co.clinton.hama.interfaces.DrawerLocker;

public class HouseDetailsFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "HouseDetailsFragment";


    private NestedScrollView nestedScrollView;

    private BottomSheetBehavior bottomSheetBehavior;

    private HouseService houseService;

    private ImageView houseDetailsImageView;

    private Dialog savedApartmentsDialog;

    private Button btnSaveApartment, btnApplyApartment, btnViewSavedApartments,
            btnHome;

    private RatingBar ratingBar;

    private String hId, houseName, descriptiom, location, bedrooms,
            rent, imageUrl;

    private TextView tvRatingsNumber, tvReviewTime;

    private  House house;

    private HousesViewModel housesViewModel;

    public static HouseDetailsFragment newInstance() {
        return new HouseDetailsFragment();
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ((MainActivity)getActivity()).getSupportActionBar().hide();
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
        getActivity().invalidateOptionsMenu();

        housesViewModel = ViewModelProviders.of(getActivity()).get(HousesViewModel.class);

        View view = inflater.inflate(R.layout.fragment_house_details, container, false);
        CollapsingToolbarLayout ctl = view.findViewById(R.id.collapsing_toolbar);
        ctl.setTitle("House Name");
        ctl.setCollapsedTitleTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        ctl.setExpandedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        toolbar.setNavigationIcon(R.drawable.ic_back_button);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        houseService = new HouseService();
        house = houseService.getHouseFromBundle(getArguments().getString("house"));

        hId = house.getId();
        houseName = house.getHouseName();
        descriptiom = house.getDescriptiom();
        location = house.getLocation();
        bedrooms = house.getBedrooms();
        rent = house.getRent();
        imageUrl = house.getImageUrl();

        List ft = new ArrayList();

        for(int y=1; y <= 11; y++){
            ft.add("Feature "+y);
        }
        //lets format the tests

        TextView features = view.findViewById(R.id.features);
        TextView features2 = view.findViewById(R.id.featuresrow2);
        formatFeatures(ft, features, features2);

        TextView location = view.findViewById(R.id.location);
        location.setText(house.getLocation());

        TextView individualRent = view.findViewById(R.id.individual_rent);
        individualRent.setText(house.getRent());

        TextView individualDesc = view.findViewById(R.id.individual_desc);
        individualDesc.setText(house.getDescriptiom());


        CollapsingToolbarLayout ctl = view.findViewById(R.id.collapsing_toolbar);
        ctl.setTitle(house.getHouseName());

        ImageView imageView = view.findViewById(R.id.houseImages);
        String imageName= house.getImageUrl();

        if(imageName.equals("1")){
            imageView.setImageResource(R.drawable.house1);
        } else if(imageName.equals("2")){
            imageView.setImageResource(R.drawable.house2);
        } else {
            imageView.setImageResource(R.drawable.house3);
        }

        final LinearLayout displayButtons = view.findViewById(R.id.display_buttons);
//        displayButtons.setVisibility(View.GONE);

        btnSaveApartment = view.findViewById(R.id.btn_save_apartment);
        btnApplyApartment = view.findViewById(R.id.btn_apply_apartment);

        ratingBar = view.findViewById(R.id.rating_bar);
        tvRatingsNumber = view.findViewById(R.id.tv_ratings_number);
        tvReviewTime = view.findViewById(R.id.tv_review_time);

        float rating = ratingBar.getRating();

        tvRatingsNumber.setText(String.valueOf(rating));

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

            }
        });

        String dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        tvReviewTime.setText(dateTime);

        btnSaveApartment.setOnClickListener(this);
        btnApplyApartment.setOnClickListener(this);

        nestedScrollView = view.findViewById(R.id.biggerScroll);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY > oldScrollY){
                    //it is moving down
                    displayButtons.setVisibility(View.VISIBLE);
                }else if(scrollY == 0) {
//                    displayButtons.setVisibility(View.GONE);
                }
            }
        });

    }


    public void formatFeatures(List features, TextView featuresView1, TextView featuresView2){
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder2 = new StringBuilder();
        int totalFeatures = (features.size() +1) / 2;
        for(int x=0; x<features.size(); x++){
            if(x < totalFeatures){
                stringBuilder.append("♦ "+features.get(x)+"<br/>");
            } else {
                stringBuilder2.append("♦ "+features.get(x)+"<br/>");
            }
        }

        //lets format the tests
        featuresView1.setText(Html.fromHtml(stringBuilder.toString()));
        featuresView2.setText(Html.fromHtml(stringBuilder2.toString()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save_apartment:
               showDialog(v);
//                savedHouse.setId(hId);
//                savedHouse.setHouseName(houseName);
//                savedHouse.setDescriptiom(descriptiom);
//                savedHouse.setLocation(location);
//                savedHouse.setBedrooms(bedrooms);
//                savedHouse.setRent(rent);
//                savedHouse.setImageUrl(imageUrl);
//
//                // Adding saved house to saved houses list
//                MainActivity.savedHousesList.add(savedHouse);
//
//                Navigation.findNavController(v).navigate(R.id.nav_saved_apartments);
//
//                // Getting an Instance of Save House
//                if(getActivity() instanceof MainActivity)
//                {
//                    ((AddRemoveCallbacks)getActivity()).onSaveHouse();
//                }

                break;
            case R.id.btn_apply_apartment:

                break;
            case R.id.btn_home:
                    Navigation.findNavController(v).navigate(R.id.nav_home);
                default:
                    break;
        }
    }

    private void showDialog(final View view) {

        savedApartmentsDialog = new Dialog(getActivity());
        savedApartmentsDialog.setContentView(R.layout.layout_dialog_saved);
        savedApartmentsDialog.show();

        btnViewSavedApartments = savedApartmentsDialog.findViewById(R.id.btn_view_saved_apartments);
        btnHome = savedApartmentsDialog.findViewById(R.id.btn_home);

        savedApartmentsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // Setting size of the dialog
        savedApartmentsDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        btnViewSavedApartments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Adding saved house to saved houses list
                housesViewModel.setSavedHouse(house);
                Navigation.findNavController(view).navigate(R.id.nav_saved_houses);

                // Getting an Instance of Save House
                if(getActivity() instanceof MainActivity)
                {
                    ((AddRemoveCallbacks)getActivity()).onSaveHouse();
                }
                savedApartmentsDialog.dismiss();
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.nav_saved_houses);
                savedApartmentsDialog.dismiss();
            }
        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getSupportActionBar().hide();
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
    }
    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).getSupportActionBar().show();
        ((DrawerLocker) getActivity()).setDrawerEnabled(true);
    }

}
