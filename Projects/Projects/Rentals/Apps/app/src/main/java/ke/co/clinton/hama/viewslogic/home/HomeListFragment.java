package ke.co.clinton.hama.viewslogic.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.List;

import ke.co.clinton.hama.R;
import ke.co.clinton.hama.dataLayer.SharedViewModels.HousesViewModel;
import ke.co.clinton.hama.dataLayer.entities.House;

public class HomeListFragment extends Fragment implements HomesAdapter.OnHouseClickedListener {
    private static final String TAG = "HomeFragment";

    private HomesAdapter homesAdapter;


    private HousesViewModel housesViewModel;

    private View root;


    private static final int TIME_INTERVAL = 2000;

    private long backPressed;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        Log.d(TAG, "onCreateView: called here "+ new Date().getTime());
        root = inflater.inflate(R.layout.fragment_home, container, false);
        // Handle on back click listeners for the home fragment
//        root.setFocusableInTouchMode(true);
//        root.requestFocus();
//        root.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                    if (keyCode == KeyEvent.KEYCODE_BACK) {
//
//                        if (backPressed + TIME_INTERVAL > System.currentTimeMillis()) {
//                            getActivity().finish();
//                            return true;
//                        } else {
//                            Toast.makeText(getActivity(), "Press back button again to exit", Toast.LENGTH_SHORT).show();
//                        }
//
//                        backPressed = System.currentTimeMillis();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//
        RecyclerView recyclerView = root.findViewById(R.id.trending_houses);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        homesAdapter = new HomesAdapter(this);
        housesViewModel = ViewModelProviders.of(this).get(HousesViewModel.class);
        housesViewModel.getHouses().observe(this, new Observer<List<House>>() {
            @Override
            public void onChanged(List<House> houses) {
                homesAdapter.setHousesRefresh(houses);
            }
        });
        recyclerView.setAdapter(homesAdapter);

        return  root;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ----- here "+ new Date().getTime());
    }

    @Override
    public void OnHouseClick(int position) {
        House house = homesAdapter.getHouseClicked(position);
        HouseDetailsFragment houseDetailsFragment = new HouseDetailsFragment();
        Bundle bundle=new Bundle();
        bundle.putString("house", house.toString());
        bundle.putString("image", house.getImageUrl());
        houseDetailsFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, houseDetailsFragment);
        transaction.addToBackStack(this.getClass().getName());
        transaction.commit();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        getActivity().getMenuInflater().inflate(R.menu.main, menu);
//        super.onCreateOptionsMenu(menu, inflater);

        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.search_action);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query != null){
                    homesAdapter.setSearchResult(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null){
                    homesAdapter.setSearchResult(newText);
                }
                return true;
            }


        });
        searchView.setOnQueryTextFocusChangeListener(new SearchView.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    //lets reset the text view
                    homesAdapter.resetList();
                }
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        if (item.getItemId() == R.id.action_account) {
//            Navigation.findNavController(root).navigate(R.id.nav_account_management);
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}