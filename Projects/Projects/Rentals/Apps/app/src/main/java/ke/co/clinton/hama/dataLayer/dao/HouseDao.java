package ke.co.clinton.hama.dataLayer.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.clinton.hama.dataLayer.entities.House;

@Dao
public interface HouseDao {

    @Insert
    void insert(House house);

    @Update
    void update(House house);

    @Delete
    void delete(House house);

    @Query("SELECT * FROM house_table")
    List<House> getAll();

    @Query("SELECT * FROM house_table")
    LiveData<List<House>> getAllHousesLiveData();

    @Query("SELECT * FROM house_table")
    LiveData<List<House>> getAllSavedHouses();

    @Query("SELECT * FROM house_table WHERE isApplied = 1 ")
    LiveData<List<House>> getAllAppliedHouses();

    @Query("DELETE FROM house_table")
    void deleteAllHouses();

    @Query("UPDATE house_table SET isSaved = 0")
    void deleteAllSavedHouses();

    @Query("UPDATE house_table SET isApplied = 0")
    void deleteAllAppliedHouses();
}
