/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.zuri.rta.connect.configs;

import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author CWEKESA
 */
@Service
public class LogsManager {

    public String addlogger(String requestId, int response_code,String Message,String LogDetailedMessage){
        StringBuilder sb=new StringBuilder();
        sb
                .append(Message)
                .append(" | RequestId =")
                .append(requestId)
                .append(" | ResponseCode =")
                .append(response_code)
                .append(" | LogDetailedMessage= ")
                .append(LogDetailedMessage);
        return sb.toString();
    }
    public  String errorMessage(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
    
}
