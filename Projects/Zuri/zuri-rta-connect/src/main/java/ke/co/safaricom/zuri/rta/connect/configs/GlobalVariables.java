package ke.co.safaricom.zuri.rta.connect.configs;

public class GlobalVariables {

    public static int ERROR_CODE_500 = 500;
    public static final String TEXT = "text";
    public static final String DETAILS = "details";
    public static final String TYPE = "type";

    public static String REQUEST_FOR_WS_CHAT = "Recieved chat from ws client";
}
