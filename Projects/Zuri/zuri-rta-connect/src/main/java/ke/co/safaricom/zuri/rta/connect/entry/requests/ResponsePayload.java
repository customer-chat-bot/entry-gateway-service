package ke.co.safaricom.zuri.rta.connect.entry.requests;

public class ResponsePayload {

    private  String message;
    private UserDetails userDetails;
    private String correlationId;

    public ResponsePayload() {
        //here its for request mapping
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponsePayload{" +
                "message='" + message + '\'' +
                ", userDetails=" + userDetails +
                '}';
    }
}
