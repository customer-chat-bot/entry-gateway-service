package ke.co.safaricom.zuri.rta.connect.configs;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestNotCompleteException extends RuntimeException {
    public RequestNotCompleteException(String s) {
        super(s);
    }
}