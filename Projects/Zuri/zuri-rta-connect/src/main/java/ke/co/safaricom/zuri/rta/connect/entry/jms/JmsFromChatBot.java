package ke.co.safaricom.zuri.rta.connect.entry.jms;

import ke.co.safaricom.zuri.rta.connect.configs.GlobalVariables;
import ke.co.safaricom.zuri.rta.connect.configs.LogsManager;
import ke.co.safaricom.zuri.rta.connect.services.datalogic.CallbackProcessor;
import ke.co.safaricom.zuri.rta.connect.services.dispatch.Dispatcher;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JmsFromChatBot {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(JmsFromChatBot.class);


    @Autowired
    CallbackProcessor callbackProcessor;


    @Autowired
    Dispatcher dispatcher;

    @RabbitListener(queues = "${rta-send-to-connect-queue}")
    public void getMessageForAI(String message) {
        try{
            logger.info("Message recieved in secure here lets move inside");
            logger.info(message);
            dispatcher.sendToWsClient("", "",  message);
        }
        catch(JSONException e){
            e.printStackTrace();
            logger.error(logs.addlogger("", GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_WS_CHAT, logs.errorMessage(e)));
        }
    }

}
