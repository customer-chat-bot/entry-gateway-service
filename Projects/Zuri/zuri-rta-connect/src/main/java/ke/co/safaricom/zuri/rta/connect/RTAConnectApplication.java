package ke.co.safaricom.zuri.rta.connect;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class RTAConnectApplication {
	public static void main(String[] args) {
		SpringApplication.run(RTAConnectApplication.class, args);
	}
}

