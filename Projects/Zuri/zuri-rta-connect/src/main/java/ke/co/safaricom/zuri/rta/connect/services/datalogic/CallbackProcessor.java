package ke.co.safaricom.zuri.rta.connect.services.datalogic;

import ke.co.safaricom.zuri.rta.connect.configs.GlobalVariables;
import ke.co.safaricom.zuri.rta.connect.configs.LogsManager;
import ke.co.safaricom.zuri.rta.connect.entry.requests.ResponsePayload;
import ke.co.safaricom.zuri.rta.connect.services.dispatch.Dispatcher;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CallbackProcessor {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(CallbackProcessor.class);

    @Autowired
    Dispatcher dispatchWs;

    public void processPayLoadFromExit(ResponsePayload responsePayload){
        try{
            logger.info("Message recieved in secure here lets move inside");
            logger.info(responsePayload.getUserDetails().getUserInfo().getBotUserId());
            dispatchWs.sendToWsClient(responsePayload.getCorrelationId(), responsePayload.getUserDetails().getUserInfo().getBotUserId(), responsePayload.getMessage());
        }
        catch(JSONException e){
            logger.error(logs.addlogger(responsePayload.getCorrelationId(), GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_WS_CHAT, logs.errorMessage(e)));
        }
    }
}
