package ke.co.safaricom.zuri.rta.connect.entry.requests;


import org.json.JSONObject;

public class UserDetails {

    private UserInfo userInfo;
    private AppsInfo appInfo;

    public UserDetails() {
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public AppsInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppsInfo botInfo) {
        this.appInfo = botInfo;
    }

    public class UserInfo{
        private String userId;
        private String botUserId;
        private String botName;
        private String botSecretId;
        private String names;
        private boolean isFirstTime;

        public UserInfo() {
        }

        public boolean isFirstTime() {
            return isFirstTime;
        }

        public void setFirstTime(boolean firstTime) {
            isFirstTime = firstTime;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getBotUserId() {
            return botUserId;
        }

        public void setBotUserId(String botUserId) {
            this.botUserId = botUserId;
        }

        public String getBotName() {
            return botName;
        }

        public void setBotName(String botName) {
            this.botName = botName;
        }

        public String getBotSecretId() {
            return botSecretId;
        }

        public void setBotSecretId(String botSecretId) {
            this.botSecretId = botSecretId;
        }

        public String getNames() {
            return names;
        }

        public void setNames(String names) {
            this.names = names;
        }

        @Override
        public String toString() {
            JSONObject user=new JSONObject();
            user.put("userId", userId);
            user.put("names", names);
            user.put("botName", botName);
            user.put("botUserId", botUserId);
            user.put("botSecretId", botSecretId);
            user.put("isfirsttime", isFirstTime);
            return user.toString();
        }
    }
    public class AppsInfo {
        private String appId;
        private String appName;
        private String tagName;
        private String appSecretId;
        private String authToken;
        private String webhook;
        private String accessToken;
        private String domain;
        private String persona;

        public AppsInfo() {
        }

        public String getTagName() {
            return tagName;
        }

        public void setTagName(String tagName) {
            this.tagName = tagName;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppSecretId() {
            return appSecretId;
        }

        public void setAppSecretId(String appSecretId) {
            this.appSecretId = appSecretId;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getWebhook() {
            return webhook;
        }

        public void setWebhook(String webhook) {
            this.webhook = webhook;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getPersona() {
            return persona;
        }

        public void setPersona(String persona) {
            this.persona = persona;
        }
        @Override
        public String toString() {
            JSONObject data = new JSONObject();
            data.put("appId", appId);
            data.put("appName", appName);
            data.put("tagName", tagName);
            data.put("accessToken", accessToken);
            data.put("appSecretId", appSecretId);
            data.put("webhook", webhook);
            data.put("authToken", authToken);
            data.put("domain", domain);
            data.put("persona", persona);
            return data.toString();
        }
    }

    @Override
    public String toString() {
        return asJSON().toString();
    }

    public JSONObject asJSON(){
        JSONObject response = new JSONObject();
        response.put("userDetails", new JSONObject()
                .put("appInfo", new JSONObject(getAppInfo().toString()))
                .put("userInfo", new JSONObject(getUserInfo().toString())));
        return response;
    }
}

