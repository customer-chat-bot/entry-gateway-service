package ke.co.safaricom.zuri.rta.connect.configs;

import org.json.JSONObject;

import java.util.Date;

public class ExceptionResponse {

    private Date timestamp;
    private String message;

    public ExceptionResponse(Date timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        JSONObject response=new JSONObject();
        response.put("timestamp",timestamp);
        response.put("message",message);
        return response.toString();
    }
}
