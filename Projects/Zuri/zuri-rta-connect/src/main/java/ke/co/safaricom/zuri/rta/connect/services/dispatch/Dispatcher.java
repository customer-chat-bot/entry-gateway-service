package ke.co.safaricom.zuri.rta.connect.services.dispatch;

import ke.co.safaricom.zuri.rta.connect.configs.GlobalVariables;
import ke.co.safaricom.zuri.rta.connect.configs.LogsManager;
import ke.co.safaricom.zuri.rta.connect.configs.QueueConfigurations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class Dispatcher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    public void sendToJMS(String trackingId, String destination, String message){
        try{
            logger.info("QueueConfigurations.queueExchangeName : "+ QueueConfigurations.queueExchangeName);
            rabbitTemplate.convertAndSend(QueueConfigurations.queueExchangeName,
                    destination, message);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sendToWsClient(String trackingId, String destination, String message){
        try{
            logger.info(destination);
            logger.info(message);
            messagingTemplate.convertAndSend("/sendtoagent/example",message);
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_WS_CHAT,logs.errorMessage(e)));
        }
    }

}
