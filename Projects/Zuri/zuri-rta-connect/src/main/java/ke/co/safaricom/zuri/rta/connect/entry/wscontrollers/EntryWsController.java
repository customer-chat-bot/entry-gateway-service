package ke.co.safaricom.zuri.rta.connect.entry.wscontrollers;

import ke.co.safaricom.zuri.rta.connect.entry.requests.WsIncomingData;
import ke.co.safaricom.zuri.rta.connect.services.datalogic.EntryWsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class EntryWsController {


    @Autowired
    EntryWsService entryWsService;
    
    @MessageMapping("/sendtoconnect")
    public void sendMessage(String wsIncomingData, @Header("simpSessionId") String sessionId) throws Exception {
        System.out.println("Session ID : "+sessionId);
        entryWsService.entry(wsIncomingData);
    }
}
