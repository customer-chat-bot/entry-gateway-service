package ke.co.safaricom.zuri.rta.connect.entry.requests;

import ke.co.safaricom.zuri.rta.connect.configs.Config;
import org.json.JSONObject;

import javax.validation.constraints.NotNull;

public class WsIncomingData {
    @NotNull(message = "Text is required" )
    private String text;
    @NotNull(message = "Token is required" )
    private String token;
    private String trackingId;
    private String profile;
    private String appName;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTrackingId() {
        return trackingId !=null? trackingId : new Config().requestID();
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getProfile() {
        return profile != null ? profile : "professional";
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAppName() {
        return appName != null ? appName : "ZuriStandalone";
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("text", text);
        jsonObject.put("token", token);
        jsonObject.put("profile", profile);
        jsonObject.put("appName", appName);
        return jsonObject.toString();
    }
}
