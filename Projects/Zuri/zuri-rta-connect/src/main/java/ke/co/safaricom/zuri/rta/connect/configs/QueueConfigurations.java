package ke.co.safaricom.zuri.rta.connect.configs;

import ke.co.safaricom.zuri.rta.connect.entry.requests.ResponsePayload;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class QueueConfigurations implements RabbitListenerConfigurer {

    public static  String queueExchangeName;
    @Value("${bot-exchange-name}")
    void setQueueExchangeName(String queueExchangeName) {
        QueueConfigurations.queueExchangeName = queueExchangeName;
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(queueExchangeName, true, true);
    }


    //chat send to rta
    @Value("${rta-send-to-connect-queue}")
    public  String botRTASendToConnectQueueName;
    @Bean
    Queue botRTASendToConnectQueue() {
        return new Queue(botRTASendToConnectQueueName, true);
    }

    @Bean
    Binding bindingForBotRTASendToConnectQueueName(Queue botRTASendToConnectQueue, DirectExchange exchange) {
        return BindingBuilder.bind(botRTASendToConnectQueue).to(exchange).with(botRTASendToConnectQueueName);
    }


    @Value("${ws-callback-message-queue}")
    public String wsCallBackMessageName;
    @Bean
    Queue wsCallBackMessage() {
        return new Queue(wsCallBackMessageName, true);
    }
    @Bean
    Binding bindingForwsCallBackMessage(Queue wsCallBackMessage, DirectExchange exchange) {
        return BindingBuilder.bind(wsCallBackMessage).to(exchange).with(wsCallBackMessageName);
    }


    @Value("${ws-send-message-queue}")
    public String wsSendMessageQueueName;
    @Bean
    Queue wsSendMessage() {
        return new Queue(wsSendMessageQueueName, true);
    }
    @Bean
    Binding bindingForwsSendMessageQueue(Queue wsSendMessage, DirectExchange exchange) {
        return BindingBuilder.bind(wsSendMessage).to(exchange).with(wsSendMessageQueueName);
    }

    @Value("${ws-auth-request-queue}")
    public String wsAuthQueueName;
    @Bean
    Queue wsAuthQueue() {
        return new Queue(wsAuthQueueName, true);
    }
    @Bean
    Binding bindingFornlpgetIntentQueue(Queue wsAuthQueue, DirectExchange exchange) {
        return BindingBuilder.bind(wsAuthQueue).to(exchange).with(wsAuthQueueName);
    }

    /* Bean for rabbitTemplate */
    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());

        return rabbitTemplate;
    }
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {

        Jackson2JsonMessageConverter jsonMessageConverter = new Jackson2JsonMessageConverter();
        jsonMessageConverter.setClassMapper(classMapper());
        return jsonMessageConverter;
    }
    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }
    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }

    @Bean
    public DefaultClassMapper classMapper()
    {
        DefaultClassMapper classMapper = new DefaultClassMapper();
        Map<String, Class<?>> idClassMapping = new HashMap<>();
        idClassMapping.put("ke.co.safaricom.webportalapp.entry.requests", ResponsePayload.class);
        classMapper.setIdClassMapping(idClassMapping);
        return classMapper;
    }

    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }
}
