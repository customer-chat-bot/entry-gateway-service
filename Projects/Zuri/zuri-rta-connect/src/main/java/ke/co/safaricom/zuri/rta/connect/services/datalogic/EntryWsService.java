package ke.co.safaricom.zuri.rta.connect.services.datalogic;

import ke.co.safaricom.zuri.rta.connect.configs.GlobalVariables;
import ke.co.safaricom.zuri.rta.connect.configs.LogsManager;
import ke.co.safaricom.zuri.rta.connect.services.dispatch.Dispatcher;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EntryWsService {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(EntryWsService.class);

    @Autowired
    Dispatcher dispatchWs;


    public String entry(String wsIncomingData) {
//        dispatchWs.sendToWsClient(wsIncomingData.getTrackingId(), wsIncomingData.getToken(), new Config().createTextData("You have reached the server..."));
        try{
            logger.info("Recievded here all the requests data aha aujana  ha aaya");
            sendToEntryService("", wsIncomingData);
        }
        catch(Exception e){
            logger.error(logs.addlogger("", GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_WS_CHAT, logs.errorMessage(e)));
        }
        return  wsIncomingData.toString();
    }

    @Value("${ws-send-message-queue}")
    String wsSendMessageQueue;

    private void sendToEntryService(String trackingId, String wsIncomingData ){
        try{
            JSONObject request = new JSONObject();
            request.put("recieved", wsIncomingData);
            request.put("server", true);
            dispatchWs.sendToWsClient(trackingId, wsSendMessageQueue, request.toString());
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_WS_CHAT, logs.errorMessage(e)));
        }
    }
}
