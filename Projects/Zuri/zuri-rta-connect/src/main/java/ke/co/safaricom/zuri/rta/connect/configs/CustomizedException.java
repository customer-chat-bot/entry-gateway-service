package ke.co.safaricom.zuri.rta.connect.configs;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;


@ControllerAdvice
@RestController
public class CustomizedException extends ResponseEntityExceptionHandler{

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) {
        ExceptionResponse exceptionResponse=new ExceptionResponse(new Date(),ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(RequestNotCompleteException.class)
    public final ResponseEntity<Object> handleRequestNotCompleteException(RequestNotCompleteException ex, WebRequest request) {
        ExceptionResponse exceptionResponse=new ExceptionResponse(new Date(),null);
        JSONObject toSend=new JSONObject(exceptionResponse.toString());
        toSend.put("details",new JSONObject(ex.getMessage()));
        return new ResponseEntity<>(toSend.toString(), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(RequestUnAuthorizedException.class)
    public final ResponseEntity<Object> handleRequestNotAuthorizedException(RequestUnAuthorizedException ex, WebRequest request) {
        ExceptionResponse exceptionResponse=new ExceptionResponse(new Date(),null);
        JSONObject toSend=new JSONObject(exceptionResponse.toString());
        toSend.put("details",new JSONObject(ex.getMessage()));
        return new ResponseEntity<>(toSend.toString(), HttpStatus.UNAUTHORIZED);
    }
}
