package ke.co.clinton.chatbot.services.datalogic;


import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.config.SharedData;
import ke.co.clinton.chatbot.dblayer.dataholders.Accounts;
import ke.co.clinton.chatbot.dblayer.dataholders.UserDetails;
import ke.co.clinton.chatbot.dblayer.dbao.AppUsersAo;
import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    AppUsersAo appUsersAo;

    @Autowired
    SharedData sharedData;
    boolean firstTimeUser = false;

    private static final  String FACEBOOK_PROFILE="https://graph.facebook.com/v2.6/";

    public UserDetails getUserForChat(long startTime, BotUserMessage botUserMessage) {
        UserDetails userDetails = null;
        try{
            Apps apps = getApps(startTime, botUserMessage);
            AppUser appUser = getAppUser(startTime, botUserMessage, apps);
            //now lets get the bot details now
            userDetails = new UserDetails(apps, appUser, firstTimeUser);
            if(botUserMessage.isSecure() && botUserMessage.getPhoneNumber() != null){
                //it means the userId is the same as the phoneNumber
                addPhoneNumber(startTime, botUserMessage.getTrackingId(), botUserMessage.getPhoneNumber(), appUser);
            }
        }
        catch(Exception e){
            logger.error(logs.addlogger(botUserMessage.getTrackingId(), GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
        return userDetails;
    }

    public Apps getApps(long startTime, BotUserMessage botUserMessage) {
        Apps apps = null;
        try{
            apps = appUsersAo.getAppForAll(botUserMessage.getBotName(), botUserMessage.getAppSecretId());
            if(apps==null) {
                apps = new Apps(botUserMessage.getBotName(), "default", botUserMessage.getAppSecretId(),  "safaricom", "professional", sharedData.getDateFromLocalDateTimeNow());
                apps = appUsersAo.addApp(apps);
            }
        }
        catch(Exception e){
            logger.error(logs.addlogger(botUserMessage.getTrackingId(), GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
        return apps;
    }

    public AppUser getAppUser(long startTime, BotUserMessage botUserMessage, Apps apps){
        AppUser appUser = null;
        try{
            appUser = appUsersAo.getOneChatUser(botUserMessage.getBotName(), botUserMessage.getAppSecretId(), botUserMessage.getUserId());
            if(appUser == null) {
                //lets create the new user now
                appUser = new AppUser();
                appUser.setNames(botUserMessage.getUserName());
                appUser.setBotUserId(botUserMessage.getUserId());
                appUser.setBotName(botUserMessage.getBotName());
                appUser.setCreated(sharedData.getDateFromLocalDateTimeNow());
                appUser.setBotSecretId(botUserMessage.getAppSecretId());
                appUser = (AppUser) appUsersAo.add(appUser);
                if(appUser.getUserId() != null){
                    firstTimeUser = true;
                }
            }
            if(appUser.getNames() == null || appUser.getNames().equalsIgnoreCase(GlobalVariables.FRIEND)) {
                String name = modifyName(botUserMessage.getTrackingId(), startTime, appUser, apps, botUserMessage.getUserName());
                if( (appUser.getNames() == null) || ( appUser.getNames() !=null && !appUser.getNames().equalsIgnoreCase(name))) {
                    appUser.setNames(name);
                    appUsersAo.update(appUser);
                }
            }
        }
        catch(Exception e){
            logger.error(logs.addlogger(botUserMessage.getTrackingId(), GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return appUser;
    }

    private String modifyName(String trackingId, long startTime, AppUser appUser, Apps apps, String names){
        return  names != null ? names : getUsername(startTime, trackingId, apps, appUser);
    }

    public String getUsername(long startTime, String trackingId, Apps app, AppUser user){
        String firstName=GlobalVariables.FRIEND;
        try{
            String name = app.getAppName();//bot_access_token
            if(name.equalsIgnoreCase(GlobalVariables.FACEBOOK)){
                String accessToken = app.getAccessToken();
                if(accessToken != null){
                    String fullUrl = FACEBOOK_PROFILE+user.getBotUserId()+"?access_token="+accessToken;
                    ResponseEntity<String> response = restTemplate.getForEntity(fullUrl,
                            String.class);
                    if (HttpStatus.OK == response.getStatusCode()) {
                        JSONObject res=new JSONObject(response.getBody());
                        if(res.has(GlobalVariables.USER_FIRST_NAME)){
                            firstName=res.getString(GlobalVariables.USER_FIRST_NAME);
                        }
                    }
                    else if (logger.isInfoEnabled() && HttpStatus.OK != response.getStatusCode()){
                        logger.info(logs.addlogger(trackingId,GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW, response.getBody(), startTime));
                    }
                }
            }
        }
        catch(Exception e){
            firstName=GlobalVariables.FRIEND;
            logger.error(logs.addlogger(trackingId,GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
        return firstName;
    }

    public AppUser addPhoneNumber(long startTime, String trackingId, String phoneNumber, AppUser appUser){
        try{
            boolean isThere = false;
            List<Accounts> accounts = appUser.getAccounts() != null ? appUser.getAccounts() : new ArrayList<>();
            for(Accounts accounts1 : accounts){
                if(accounts1.getPhoneNumber().equalsIgnoreCase(phoneNumber)){
                    accounts1.setCreated(accounts1.getCreated());
                    isThere = true;
                    break;
                }
            }
            if(!isThere) {
                //lets add it because its not there
                accounts.add(new Accounts(phoneNumber, sharedData.getDateFromLocalDateTimeNow()));
                appUser.setAccounts(accounts);
                appUsersAo.update(appUser);
            }

        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
        return appUser;
    }

}
