package ke.co.clinton.chatbot.services.datalogic;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.dblayer.dataholders.UserDetails;
import ke.co.clinton.chatbot.services.dispatcher.Dispatcher;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.config.SharedData;
import ke.co.clinton.chatbot.dblayer.dbao.AppUsersAo;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SharedResource {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(SharedResource.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    AppUsersAo appUsersAo;

    @Autowired
    SharedData sharedData;

    @Autowired
    UserService userService;

    @Autowired
    Dispatcher dispatcher;

    @Value("${session-get-queue}")
    String getSessionQueue;


    @Value("${requests-queue-offloader}")
    String requestsQueueOffloader;

    public JSONObject queueRequest(long startTime, String trackingId, String appname, String data){
        //theres no try catch here cause its not doing any manipulation to data
        JSONObject request = new JSONObject();
        request.put("trackingId", trackingId)
                .put("appname", appname)
                .put("startTime", startTime)
                .put("data", data);
        dispatcher.sendJMS(startTime, trackingId, requestsQueueOffloader, request.toString());
        return  request;
    }

    public BotUserMessage createBotUserInfo(long startTime, String trackingId, BotUserMessage botUserMessage){
        try{
            UserDetails userDetails = userService.getUserForChat(startTime, botUserMessage);
            botUserMessage.setUserDetails(userDetails);
            //lets get the session details now
            dispatcher.sendToSessionForEnrichment(startTime, trackingId, botUserMessage.asJSON());
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
        return  botUserMessage;
    }



}
