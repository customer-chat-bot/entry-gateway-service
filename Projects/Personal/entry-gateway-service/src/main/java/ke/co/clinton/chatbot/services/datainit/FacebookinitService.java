/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.services.datainit;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.entry.requests.BotResponse;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import ke.co.clinton.chatbot.services.datalogic.UserMessageProcessor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FacebookinitService {

    private static final String VERIFY_TOKEN ="safaricom_web_test_bot";

    @Autowired
    SharedResource sharedResource;

    @Autowired
    Config config;

    @Autowired
    UserMessageProcessor userMessageProcessor;
    
    public String processInit(String token,String chal) {
        JSONObject response=new JSONObject();
        if(token==null){
            response.put(GlobalVariables.RESPONSE_CODE, GlobalVariables.ERROR_CODE_99);
            response.put(GlobalVariables.DESCRIPTION, "Error in the request as no required parameters added");
            return response.toString();
        }
        else{
            if(token.equals(VERIFY_TOKEN)){
                response.put(GlobalVariables.RESPONSE_CODE, GlobalVariables.SUCCESS_CODE_00);
                response.put(GlobalVariables.DESCRIPTION, chal);
                return chal;
            }
            else{
                response.put(GlobalVariables.RESPONSE_CODE, GlobalVariables.ERROR_CODE_99);
                response.put(GlobalVariables.DESCRIPTION, "Error in the request as no match from facebook");
                return response.toString();
            }
        }
    }

    public BotResponse queueRequestRecieved(String data) {
        String trackingId = config.getTrackingId();
        long startTime = config.getTimeNow();
        JSONObject response = sharedResource.queueRequest(startTime,  trackingId, GlobalVariables.FACEBOOK , data);
        return new BotResponse(200, response);
    }

    public BotUserMessage processAfterQueueing(long startTime, String trackingId, String input) {
        BotUserMessage botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, input);
        return sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
    }   
}
