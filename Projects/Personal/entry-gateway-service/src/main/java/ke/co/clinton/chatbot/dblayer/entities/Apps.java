package ke.co.clinton.chatbot.dblayer.entities;

import org.json.JSONObject;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Apps {

    @Id
    private String appId;
    private String appName;
    private String tagName;
    private String appSecretId;
    private String authToken;
    private String webhook;
    private String accessToken;
    private String domain;
    private String persona;
    private Date created;

    public Apps(String appName, String tagName, String appSecretId, String domain, String persona, Date created) {
        this.appName = appName;
        this.tagName = tagName;
        this.appSecretId = appSecretId;
        this.domain = domain;
        this.persona = persona;
        this.created = created;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppName() {
        return appName;
    }

    public String getAppSecretId() {
        return appSecretId;
    }

    public String getAccessToken() {
        return accessToken;
    }


    public JSONObject asJson(){
        JSONObject data = new JSONObject();
        data.put("appId", appId);
        data.put("appName", appName);
        data.put("tagName", tagName);
        data.put("accessToken", accessToken);
        data.put("appSecretId", appSecretId);
        data.put("webhook", webhook);
        data.put("authToken", authToken);
        data.put("domain", domain);
        data.put("persona", persona);
        return data;
    }

    @Override
    public String toString() {
        return asJson().toString();
    }
}
