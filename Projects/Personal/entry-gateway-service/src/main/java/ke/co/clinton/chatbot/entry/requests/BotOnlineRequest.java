package ke.co.clinton.chatbot.entry.requests;

public class BotOnlineRequest {

    String trackingId;
    String appname;
    String data;
    long startTime;

    public BotOnlineRequest() {
    }

    public BotOnlineRequest(String trackingId, String appname, String data, long startTime) {
        this.trackingId = trackingId;
        this.appname = appname;
        this.data = data;
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BotOnlineRequest{" +
                "trackingId='" + trackingId + '\'' +
                ", appname='" + appname + '\'' +
                ", data='" + data + '\'' +
                ", startTime=" + startTime +
                '}';
    }
}
