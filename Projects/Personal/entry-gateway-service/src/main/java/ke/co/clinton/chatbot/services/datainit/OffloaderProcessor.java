package ke.co.clinton.chatbot.services.datainit;

import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.entry.requests.BotOnlineRequest;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OffloaderProcessor {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(OffloaderProcessor.class);

    @Autowired
    FacebookinitService facebookinitService;
    @Autowired
    TelegramInitService telegramInitService;
    @Autowired
    Config config;

    public BotUserMessage processRequestFromAPP(BotOnlineRequest message) {
        BotUserMessage botUserMessage = null;
        long startTime = message != null ? message.getStartTime() : config.getTimeNow();
        String trackingId = message != null ? message.getTrackingId() : new Config().getTrackingId();
        try{
            String appName = message != null ? message.getAppname() : "testApp";
            String data = message != null ? message.getData() : null;
            switch (appName) {
                case GlobalVariables.FACEBOOK:
                    botUserMessage = facebookinitService.processAfterQueueing(startTime, trackingId, data);
                    break;
                case GlobalVariables.TELEGRAM:
                    botUserMessage = telegramInitService.processAfterQueueing(startTime, trackingId,
                            new JSONObject(data).getString(GlobalVariables.ID), data);
                    break;
                default:
                    if(logger.isWarnEnabled()){
                        logger.warn(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, appName, startTime));
                    }
            }
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return botUserMessage;
    }

}
