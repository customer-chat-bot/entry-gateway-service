/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.entry.controllers;

import ke.co.clinton.chatbot.config.SharedData;
import ke.co.clinton.chatbot.services.datainit.FacebookinitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/app/facebook",produces="application/json")
public class FacebookController {

    @Autowired
    private FacebookinitService facebookservice;

    @Autowired
    SharedData sharedData;
    
    @PostMapping
    public ResponseEntity<String> getBotbydetails(@RequestBody String req) {
        return  sharedData.marshalResponse(facebookservice.queueRequestRecieved(req));
    }
    //i need to add a response entity class
    @GetMapping
    public String getInitializer(@RequestParam("hub.verify_token") String token, @RequestParam("hub.challenge") String chal) {
        return facebookservice.processInit(token, chal);
    }
    
}

