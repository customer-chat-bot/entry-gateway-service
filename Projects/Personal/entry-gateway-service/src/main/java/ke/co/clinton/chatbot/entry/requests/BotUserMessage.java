package ke.co.clinton.chatbot.entry.requests;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.dblayer.dataholders.UserDetails;
import org.json.JSONObject;

public class BotUserMessage {
    private String trackingId;
    private String userId;
    private String userName;
    private String botName;
    private String appSecretId;
    private String message;
    private boolean wrongInput;
    boolean secure;
    private String phoneNumber;

    private String fileAvailable;
    private String fileUrl;
    private String fileType;
    private UserDetails userDetails;

    public BotUserMessage(String userId) {
        this.userId = userId;
    }


    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setBotName(String botName) {
        this.botName = botName;
    }

    public void setAppSecretId(String appSecretId) {
        this.appSecretId = appSecretId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setWrongInput(boolean wrongInput) {
        this.wrongInput = wrongInput;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setFileAvailable(String fileAvailable) {
        this.fileAvailable = fileAvailable;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getBotName() {
        return botName;
    }

    public String getAppSecretId() {
        return appSecretId;
    }

    public String getMessage() {
        return message;
    }

    public boolean isWrongInput() {
        return wrongInput;
    }

    public boolean isSecure() {
        return secure;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFileAvailable() {
        return fileAvailable;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public String getFileType() {
        return fileType;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public JSONObject asJSON(){
        JSONObject req = new JSONObject(userDetails.toString());
        req.put(GlobalVariables.ID, userDetails.getUserInfo().getUserId());
        req.put(GlobalVariables.MESSAGE, message);
        req.put(GlobalVariables.WRONG_INPUT, wrongInput);
        req.put(GlobalVariables.CORRELATION_ID, trackingId); //added for logging purposes
        return req;
    }

    @Override
    public String toString() {
        return asJSON().toString();
    }

    //lets user the builder pattern now

    public static class Creator {

        private String creatortrackingId;
        private String creatoruserId;
        private String creatoruserName;
        private String creatorbotName;
        private String creatorappSecretId;
        private String creatormessage;
        private boolean creatorwrongInput;
        boolean creatorsecure;
        private String creatorphoneNumber;

        private String creatorfileAvailable;
        private String creatorfileUrl;
        private String creatorfileType;

        public Creator(String userId){
            this.creatoruserId = userId;
        }

        public Creator withTrackingId(String trackingId) {
            this.creatortrackingId = trackingId;
            return this;
        }

        public Creator withUserName(String userName) {
            this.creatoruserName = userName;
            return this;
        }

        public Creator withBotName(String botName) {
            this.creatorbotName = botName;
            return this;
        }

        public Creator withAppSecretId(String appSecretId) {
            this.creatorappSecretId = appSecretId;
            return this;
        }

        public Creator withMessage(String message) {
            this.creatormessage = message;
            return this;
        }

        public Creator withWrongInput(boolean wrongInput) {
            this.creatorwrongInput = wrongInput;
            return this;
        }

        public Creator withSecure(boolean secure) {
            this.creatorsecure = secure;
            return this;
        }

        public Creator withPhoneNumber(String phoneNumber) {
            this.creatorphoneNumber = phoneNumber;
            return this;
        }

        public Creator withFileAvailable(String fileAvailable) {
            this.creatorfileAvailable = fileAvailable;
            return this;
        }

        public Creator withFileUrl(String fileUrl) {
            this.creatorfileUrl = fileUrl;
            return this;
        }

        public Creator withFileType(String fileType) {
            this.creatorfileType = fileType;
            return this;
        }

        public BotUserMessage build(){
            BotUserMessage botUserMessage = new BotUserMessage(creatoruserId);
            botUserMessage.setTrackingId(creatortrackingId);
            botUserMessage.setUserName(creatoruserName);
            botUserMessage.setBotName(creatorbotName);
            botUserMessage.setAppSecretId(creatorappSecretId);
            botUserMessage.setMessage(creatormessage);
            botUserMessage.setWrongInput(creatorwrongInput);
            botUserMessage.setSecure(creatorsecure);
            botUserMessage.setPhoneNumber(creatorphoneNumber);
            botUserMessage.setFileAvailable(creatorfileAvailable);
            botUserMessage.setFileUrl(creatorfileUrl);
            botUserMessage.setFileType(creatorfileType);
            return botUserMessage;
        }
    }
}
