package ke.co.clinton.chatbot.entry.controllers;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.entry.requests.BotResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class SharedControllerDetails {

    public ResponseEntity<String> marshalResponse(BotResponse botResponse){
        if(botResponse.getResponseCode() == GlobalVariables.SUCCESS_CODE_200) {
            return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.OK);
        }
        else if(botResponse.getResponseCode() == GlobalVariables.ERROR_CODE_500){
            return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.OK);
    }
}
