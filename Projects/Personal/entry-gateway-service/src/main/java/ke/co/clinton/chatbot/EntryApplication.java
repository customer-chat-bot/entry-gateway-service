package ke.co.clinton.chatbot;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableRabbit
@EnableAsync
@SpringBootApplication
public class EntryApplication{
    public static void main(String[] args) {
        SpringApplication.run(EntryApplication.class, args);
    }
}