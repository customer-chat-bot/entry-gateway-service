package ke.co.clinton.chatbot.config;

import ke.co.clinton.chatbot.entry.requests.BotOnlineRequest;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class QueueConfigurations implements RabbitListenerConfigurer {



    public static  String queueExchangeName;
    @Value("${bot-exchange-name}")
    void setQueueExchangeName(String queueExchangeName) {
        QueueConfigurations.queueExchangeName = queueExchangeName;
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(queueExchangeName, true, true);
    }

    @Value("${bot-metrics-queue}")
    public  String metricsCounterQueueName;
    @Bean
    Queue metricsCounterQueue() {
        return new Queue(metricsCounterQueueName, true);
    }
    @Bean
    Binding bindingFormetricsCounterQueue(Queue metricsCounterQueue, DirectExchange exchange) {
        return BindingBuilder.bind(metricsCounterQueue).to(exchange).with(metricsCounterQueueName);
    }

    @Value("${bot-chat-saving-queue}")
    public String botChatSavingQueueName;
    @Bean
    Queue botChatSavingQueue() {
        return new Queue(botChatSavingQueueName, true);
    }
    @Bean
    Binding bindingForbotChatSavingQueue(Queue botChatSavingQueue, DirectExchange exchange) {
        return BindingBuilder.bind(botChatSavingQueue).to(exchange).with(botChatSavingQueueName);
    }


    @Value("${session-get-queue}")
    public String sessionGetQueueName;
    @Bean
    Queue sessionGetQueue() {
        return new Queue(sessionGetQueueName, true);
    }
    @Bean
    Binding bindingForsessionGetQueue(Queue sessionGetQueue, DirectExchange exchange) {
        return BindingBuilder.bind(sessionGetQueue).to(exchange).with(sessionGetQueueName);
    }




    @Value("${bot-ai-sendandwait-queue}")
    public String botAiSendAndWaitQueueName;
    @Bean
    Queue botAiSendAndWaitQueue() {
        return new Queue(botAiSendAndWaitQueueName, true);
    }
    @Bean
    Binding bindingForbotAiSendAndWaitQueue(Queue botAiSendAndWaitQueue, DirectExchange exchange) {
        return BindingBuilder.bind(botAiSendAndWaitQueue).to(exchange).with(botAiSendAndWaitQueueName);
    }



    @Value("${ws-send-message-queue}")
    public String wsSendMessageQueueName;
    @Bean
    Queue wsSendMessageQueue() {
        return new Queue(wsSendMessageQueueName, true);
    }
    @Bean
    Binding bindingForwsSendMessageQueue(Queue wsSendMessageQueue, DirectExchange exchange) {
        return BindingBuilder.bind(wsSendMessageQueue).to(exchange).with(wsSendMessageQueueName);
    }


    @Value("${ws-auth-request-queue}")
    public String wsAuthQueueName;
    @Bean
    Queue wsAuthQueue() {
        return new Queue(wsAuthQueueName, true);
    }
    @Bean
    Binding bindingFornlpgetIntentQueue(Queue wsAuthQueue, DirectExchange exchange) {
        return BindingBuilder.bind(wsAuthQueue).to(exchange).with(wsAuthQueueName);
    }


    @Value("${requests-queue-offloader}")
    public String requestsQueueOffloader;
    @Bean
    Queue requestsQueueOffloaderQueue() {
        return new Queue(requestsQueueOffloader, true);
    }
    @Bean
    Binding bindingForRequestsQueueOffloaderQueue(Queue requestsQueueOffloaderQueue, DirectExchange exchange) {
        return BindingBuilder.bind(requestsQueueOffloaderQueue).to(exchange).with(requestsQueueOffloader);
    }




    /* Bean for rabbitTemplate */
    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {

        Jackson2JsonMessageConverter jsonMessageConverter = new Jackson2JsonMessageConverter();
        jsonMessageConverter.setClassMapper(classMapper());
        return jsonMessageConverter;
    }
    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }
    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }

    @Bean
    public DefaultClassMapper classMapper()
    {
        DefaultClassMapper classMapper = new DefaultClassMapper();
        Map<String, Class<?>> idClassMapping = new HashMap<>();
        idClassMapping.put("ke.co.safaricom.entry.requests.BotOnlineRequestn", BotOnlineRequest.class);

        classMapper.setIdClassMapping(idClassMapping);
        return classMapper;
    }
    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }
}
