/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.config;

import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

/**
 *
 * @author CWEKESA
 */
@Service
public class LogsManager {

    public String addlogger(String requestId, int responseCode,String Message,String LogDetailedMessage, long startTime){
        StringBuilder sb=new StringBuilder();
        sb
                .append(Message)
                .append(" | RequestId =")
                .append(requestId)
                .append(" | ResponseCode =")
                .append(responseCode)
                .append(" | LogDetailedMessage = ")
                .append(LogDetailedMessage)
                .append(" | StartTime = ")
                .append(startTime)
                .append(" | TimeNow = ")
                .append(getTimeNow());
        return sb.toString();
    }

    public  String errorMessage(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    private long getTimeNow(){
        long epoch = 0;
        try{
            LocalDateTime localNow = LocalDateTime.now(TimeZone.getTimeZone(GlobalVariables.AFRICA_NAIROBI).toZoneId());
            ZoneId zoneId = ZoneId.of(GlobalVariables.AFRICA_NAIROBI);
            epoch = localNow.atZone(zoneId).toInstant().toEpochMilli();
        }
        catch(Exception e){

        }
        return epoch;
    }
    
}
