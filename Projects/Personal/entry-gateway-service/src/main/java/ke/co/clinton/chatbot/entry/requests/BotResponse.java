package ke.co.clinton.chatbot.entry.requests;

import org.json.JSONObject;

public class BotResponse {

    private int responseCode;
    private JSONObject response;

    public BotResponse(int responseCode, JSONObject response) {
        this.responseCode = responseCode;
        this.response = response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public JSONObject getResponse() {
        return response;
    }

    public void setResponse(JSONObject response) {
        this.response = response;
    }
}
