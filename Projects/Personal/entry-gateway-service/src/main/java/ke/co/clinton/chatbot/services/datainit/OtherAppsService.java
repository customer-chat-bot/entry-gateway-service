/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.services.datainit;

import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.config.ResponsesMarshaller;
import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.entry.requests.BotAppRequest;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import ke.co.clinton.chatbot.entry.requests.OtherAppMessageRequest;
import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.services.dispatcher.Dispatcher;
import ke.co.clinton.chatbot.dblayer.dbao.AppUsersAo;
import ke.co.clinton.chatbot.entry.requests.BotResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class OtherAppsService {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(OtherAppsService.class);


    @Autowired
    Dispatcher dispatcher;

    @Autowired
    private ResponsesMarshaller responsesMarshaller;

    @Autowired
    Config config;

    @Autowired
    AppUsersAo appUsersAo;
    @Autowired
    SharedResource sharedResource;

    public BotResponse addApp(BotAppRequest request){
        int responseCode = 500;
        JSONObject response = new JSONObject();
        String trackingId = config.getTrackingId();
        long startTime= config.getTimeNow();
        try{
            Apps apps = appUsersAo.getAppForAll(request.getAppName(), request.getSecretId());
            if(apps == null) {
                String authToken=Config.nextToken();
                apps = new Apps(request.getAppName(), Config.nextToken(), request.getSecretId(), "safaricom", "professional", new Date());
                apps.setAuthToken(authToken);
                apps = appUsersAo.addApp(apps);
                if(apps.getAppId() != null) {
                    responseCode = GlobalVariables.SUCCESS_CODE_200;
                    response.put("authToken", authToken);
                }
                else {
                    response.put(GlobalVariables.MESSAGE, "Server error");
                }
            }
            else {
                response.put(GlobalVariables.MESSAGE, "Already added");
            }
        }
        catch(JSONException e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return new BotResponse(responseCode, response);
    }

    public BotResponse sendMessage(String authToken, OtherAppMessageRequest request) {
        int code = GlobalVariables.SUCCESS_CODE_200;
        JSONObject response;
        String trackingId = new Config().getTrackingId();
        long startTime= config.getTimeNow();
        try{
            //lets first validate the bot here
            Apps apps = appUsersAo.getAppByToken(authToken);
            if (apps == null) {
                code = 401;
                response=responsesMarshaller.error(401,new JSONArray().put(GlobalVariables.ERROR_UNAUTHORISED));
            }
            else {
                BotUserMessage botUserMessage =  new BotUserMessage.Creator(String.valueOf(request.getUserId()))
                        .withTrackingId(trackingId)
                        .withUserName(GlobalVariables.FRIEND)
                        .withMessage(request.getMessage())
                        .withBotName(apps.getAppName())
                        .withAppSecretId(apps.getAppSecretId())
                        .build();
                botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
                response = responsesMarshaller.success("sent");
                response.put("data", botUserMessage.asJSON());
            }
        }
        catch(Exception e){
            code = 500;
            response=responsesMarshaller.error(500,new JSONArray().put(GlobalVariables.ERROR_CODE_99_DESC));
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return new BotResponse(code, response);
    }

}
