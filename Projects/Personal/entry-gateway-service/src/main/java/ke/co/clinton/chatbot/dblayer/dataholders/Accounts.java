package ke.co.clinton.chatbot.dblayer.dataholders;

import java.util.Date;

public class Accounts {
    private String phoneNumber;
    private Date created;

    public Accounts(String phoneNumber, Date created) {
        this.phoneNumber = phoneNumber;
        this.created = created;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getCreated() {
        return created;
    }
}
