package ke.co.clinton.chatbot.dblayer.entities;

import ke.co.clinton.chatbot.dblayer.dataholders.Accounts;
import ke.co.clinton.chatbot.dblayer.dataholders.Feedback;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.Date;
import java.util.List;

public class AppUser {

    private static final long serialVersionUID = 1L;
    @Id
    private String userId;
    private String botUserId;
    private String botName;
    private String botSecretId;
    private String names;
    private String persona;
    private List<Accounts> accounts;
    private  List<Feedback> feedbacks;
    private Feedback lastFeedback;
    private Date created;
    private int importTag;

    //lets ignore the field during persistence
    @Transient
    private boolean firstTime;

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBotUserId() {
        return botUserId;
    }

    public void setBotUserId(String botUserId) {
        this.botUserId = botUserId;
    }

    public String getBotName() {
        return botName;
    }

    public void setBotName(String botName) {
        this.botName = botName;
    }

    public String getBotSecretId() {
        return botSecretId;
    }

    public void setBotSecretId(String botSecretId) {
        this.botSecretId = botSecretId;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public List<Accounts> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Accounts> accounts) {
        this.accounts = accounts;
    }


    public void setCreated(Date created) {
        this.created = created;
    }

    public JSONObject asJson() {
        JSONObject user=new JSONObject();
        user.put("userId", userId);
        user.put("names", names);
        user.put("botName", botName);
        user.put("botUserId", botUserId);
        user.put("botSecretId", botSecretId);
        user.put("isfirsttime", firstTime);
        return user;
    }

    @Override
    public String toString() {
        return asJson().toString();
    }
}
