package ke.co.clinton.chatbot.dblayer.repos;

import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepo extends MongoRepository<AppUser, String> {

    @Query("{$and:[ {'botName': ?0},{'botSecretId': ?1},{'botUserId': ?2}]}")
    Optional<AppUser> findUserByNameAndSecretID(String botName, String botSecretId, String botUserId);

}
