/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.entry.controllers;

import ke.co.clinton.chatbot.config.SharedData;
import ke.co.clinton.chatbot.entry.requests.BotAppRequest;
import ke.co.clinton.chatbot.entry.requests.OtherAppMessageRequest;
import ke.co.clinton.chatbot.services.datainit.OtherAppsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/router",produces="application/json")
public class OtherAppsController {

    @Autowired
    private OtherAppsService otherAppsService;
    @Autowired
    SharedData sharedData;
    
    @PostMapping(value = "/create")
    public ResponseEntity<String> createBot(@Valid @RequestBody BotAppRequest req, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            sharedData.emptyRequests(bindingResult);
        }
        return sharedData.marshalResponse(otherAppsService.addApp(req));
    }


    @PostMapping(value = "/{authToken}/sendmessage")
    public ResponseEntity<String> sendMessage(@PathVariable String authToken, @Valid @RequestBody OtherAppMessageRequest req, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return sharedData.requestIncomplete(bindingResult);
        }
        return  sharedData.marshalResponse(otherAppsService.sendMessage(authToken, req));
    }

}

