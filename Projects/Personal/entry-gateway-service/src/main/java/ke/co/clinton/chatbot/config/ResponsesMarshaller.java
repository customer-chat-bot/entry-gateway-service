package ke.co.clinton.chatbot.config;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 *
 * @author cwekesa
 */
@Service
public class ResponsesMarshaller {
    private final String code="response_code";
    private final String desc="description";
    private final String error="errors";
    private final String success="success";
    public JSONObject success(String message){
        return new JSONObject()
                .put(code, 0)
                .put(desc, message)
                .put(success, true);
    }
    public JSONObject error(JSONArray message){
        return new JSONObject()
                .put(error, message);
    }
    public JSONObject error(String message){
        JSONArray errors=new JSONArray();
        errors.put(message);
        return new JSONObject()
                .put(error, errors);
    }

    public JSONObject error(int cd,JSONArray message) {
        return new JSONObject()
                .put(code, cd)
                .put(error, true)
                .put(desc, message);
    }
}