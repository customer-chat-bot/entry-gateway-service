/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.services.dispatcher;


import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.config.QueueConfigurations;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class Dispatcher {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(Dispatcher.class);


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${bot-ai-send-callback-queue}")
    String botAiSendCallbackEndpoint;
    @Value("${session-get-queue}")
    private String sessionGetQueueName;
    @Value("${bot-chat-saving-queue}")
    private  String botChatSavingQueue;
    
    public void sendToSessionForEnrichment(long startTime, String requestId, JSONObject message){
        if(logger.isInfoEnabled()){
            logger.info(logs.addlogger(requestId, GlobalVariables.SUCCESS_CODE_200,GlobalVariables.REQUEST_FOR_CHAT_FLOW,  message.toString(), startTime));
        }
        sendJMS(startTime, requestId, sessionGetQueueName, message.toString());
        sendToMetricsAndChats(startTime, requestId, message);
    }

    public void sendToMetricsAndChats(long startTime, String requestId, JSONObject message){
        try{
            message.put(GlobalVariables.TYPE, "incoming")
                    .put("userType", GlobalVariables.USER)
                    .put("userLabel", GlobalVariables.USER)
                    .put("timeSubmitted", new Config().getTimeNow())
                    .put(GlobalVariables.MESSAGE, new JSONObject()
                            .put(GlobalVariables.TYPE, "text")
                            .put(GlobalVariables.DETAILS, message.getString(GlobalVariables.MESSAGE)));
            sendJMS(startTime, requestId, botChatSavingQueue, message.toString());
        }catch(Exception e){
            logger.error(logs.addlogger(requestId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
    }

    public void sendJMS(long startTime, String requestId, String queueName, String message){
        try{
            rabbitTemplate.convertAndSend(QueueConfigurations.queueExchangeName,
                    queueName, message);
        }catch(Exception e){
            logger.error(logs.addlogger(requestId,GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
    }

}
