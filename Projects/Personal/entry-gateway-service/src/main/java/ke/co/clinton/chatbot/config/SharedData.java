package ke.co.clinton.chatbot.config;

import ke.co.clinton.chatbot.entry.requests.BotResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class SharedData {

    @Autowired
    private ResponsesMarshaller responses;

    public String emptyRequests(BindingResult bindingResult){
        List errors=bindingResult.getAllErrors();
        JSONArray response=new JSONArray();
        errors.forEach(v->response.put(new JSONObject(v).getString("defaultMessage")));
        throw new RequestNotCompleteException(responses.error(response).toString());
    }

    public ResponseEntity<String> requestIncomplete(BindingResult bindingResult){
        List errors=bindingResult.getAllErrors();
        JSONArray response=new JSONArray();
        errors.forEach(v->response.put(new JSONObject(v).getString("defaultMessage")));
        return new ResponseEntity<>(responses.error(response).toString(), HttpStatus.BAD_REQUEST);
    }


    public Date getDateFromLocalDateTimeNow(){
        LocalDateTime localDateTime =LocalDateTime.now(TimeZone.getTimeZone(GlobalVariables.NAIROBI).toZoneId());
        return  Date.from(localDateTime.atZone(TimeZone.getTimeZone(GlobalVariables.NAIROBI).toZoneId()).toInstant());
    }

    public ResponseEntity<String> marshalResponse(BotResponse botResponse){
        if(botResponse.getResponseCode() == GlobalVariables.SUCCESS_CODE_200) {
            return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.OK);
        }
        else if(botResponse.getResponseCode() == GlobalVariables.ERROR_CODE_500){
            return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else if(botResponse.getResponseCode() == GlobalVariables.ERROR_CODE_401){
            return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(botResponse.getResponse().toString(), HttpStatus.OK);
    }
}
