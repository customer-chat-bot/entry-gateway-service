package ke.co.clinton.chatbot.services.datalogic;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMessageProcessor {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(UserMessageProcessor.class);

    public BotUserMessage getMessageFromFacebook(long startTime, String trackingId, String input){
        BotUserMessage botUserMessage = null;
        try{
            JSONObject request=new JSONObject(input);
            JSONObject entry=new JSONObject(request.getJSONArray(GlobalVariables.ENTRY).get(0).toString());
            JSONObject messaging=new JSONObject(entry.getJSONArray(GlobalVariables.MESSAGING).get(0).toString());
            //now we get individual details
            JSONObject sender=messaging.getJSONObject(GlobalVariables.SENDER);
            JSONObject pageId=messaging.getJSONObject(GlobalVariables.RECIPIENT);
            String userinput="n/a";
            boolean wrongInput = false;
            if(messaging.has(GlobalVariables.MESSAGE)) {
                JSONObject message=messaging.getJSONObject(GlobalVariables.MESSAGE);
                if(message.has(GlobalVariables.TEXT)) {
                    //lets check for quick replies then
                    userinput = (message.has(GlobalVariables.QUICK_REPLY)) ? message.getJSONObject(GlobalVariables.QUICK_REPLY).getString(GlobalVariables.PAYLOAD) : message.getString(GlobalVariables.TEXT);
                } else {
                    wrongInput = true;
                }
            }
            //lets check for buttons now
            userinput = (messaging.has(GlobalVariables.POSTBACK)) ? messaging.getJSONObject(GlobalVariables.POSTBACK).getString(GlobalVariables.PAYLOAD) : userinput;

            botUserMessage =  new BotUserMessage.Creator(sender.getString(GlobalVariables.ID))
                    .withTrackingId(trackingId)
                    .withSecure(false)
                    .withMessage(userinput)
                    .withBotName(GlobalVariables.FACEBOOK)
                    .withAppSecretId(pageId.getString(GlobalVariables.ID))
                    .withWrongInput(wrongInput)
                    .build();
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId,GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }

        return botUserMessage;
    }

    public BotUserMessage getMessageFromTelegram(long startTime, String trackingId, String botId,String input){
        BotUserMessage botUserMessage = null;
        try{
            JSONObject request=new JSONObject(input);
            JSONObject messagebody=request.getJSONObject(GlobalVariables.MESSAGE);
            int senderid=messagebody.getJSONObject(GlobalVariables.FROM).getInt(GlobalVariables.ID);
            String first="";
            String second="";
            first = (messagebody.getJSONObject(GlobalVariables.FROM).has(GlobalVariables.FIRST_NAME)) ?
                    messagebody.getJSONObject(GlobalVariables.FROM).getString(GlobalVariables.FIRST_NAME) : "";
            second = (messagebody.getJSONObject(GlobalVariables.FROM).has(GlobalVariables.LAST_NAME)) ?
                    messagebody.getJSONObject(GlobalVariables.FROM).getString(GlobalVariables.LAST_NAME) : "";
            String names=first+" "+second;
            String text =  (messagebody.has(GlobalVariables.ENTITIES)) ? messagebody.getString(GlobalVariables.TEXT) : null;
            text = messagebody.has(GlobalVariables.TEXT) ? messagebody.getString(GlobalVariables.TEXT) : text;
            text = (text != null && text.equalsIgnoreCase("/start")) ? "hi" : text;
            boolean wrongInput = text == null;

            botUserMessage =  new BotUserMessage.Creator(String.valueOf(senderid))
                    .withTrackingId(trackingId)
                    .withUserName(names)
                    .withSecure(false)
                    .withMessage(text)
                    .withBotName(GlobalVariables.TELEGRAM)
                    .withAppSecretId(botId)
                    .withWrongInput(wrongInput)
                    .build();
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return botUserMessage;
    }

    public BotUserMessage getMessageFromWebSockets(long startTime, String trackingId, String input){
        BotUserMessage botUserMessage = null;
        try{
            JSONObject request=new JSONObject(input);
            String user = request.has("name") ? request.getString("name") : GlobalVariables.FRIEND;
            String phoneNumber = request.has("phoneNumber") ? request.getString("phoneNumber") : null;
            botUserMessage =  new BotUserMessage.Creator(request.getString(GlobalVariables.WEBCHAT_USER))
                    .withTrackingId(trackingId)
                    .withUserName(user)
                    .withSecure(false)
                    .withMessage(request.getString(GlobalVariables.WEBCHAT_TEXT))
                    .withBotName(GlobalVariables.WEBCHAT)
                    .withAppSecretId("1")
                    .withPhoneNumber(phoneNumber)
                    .withWrongInput(false)
                    .build();
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), startTime));
        }
        return botUserMessage;
    }
}
