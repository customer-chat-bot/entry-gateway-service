package ke.co.clinton.chatbot.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestNotCompleteException extends RuntimeException {
    public RequestNotCompleteException(String s) {
        super(s);
    }
}