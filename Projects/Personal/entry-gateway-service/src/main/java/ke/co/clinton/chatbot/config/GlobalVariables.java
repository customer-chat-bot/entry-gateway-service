package ke.co.clinton.chatbot.config;

public class GlobalVariables {

    public static final String DETAILS = "details";
    public static final String USER = "user";
    public static int SUCCESS_CODE_00 = 0;
    public static int SUCCESS_CODE_200 = 200;
    public static int ERROR_CODE_99 = 99;
    public static int ERROR_CODE_500 = 500;
    public static int ERROR_CODE_401 = 401;
    public static String ERROR_CODE_99_DESC = "You set an empty request";
    public static String ERROR_UNAUTHORISED = "Your request is unauthorised";
    public static String ENTRY = "entry";
    public static String SENDER = "sender";
    public static String RECIPIENT = "recipient";
    public static String MESSAGE = "message";
    public static String MESSAGING = "messaging";
    public static String TEXT = "text";
    public static String QUICK_REPLY = "quick_reply";
    public static String PAYLOAD = "payload";
    public static String POSTBACK = "postback";
    public static String ID = "id";
    public static String WRONG_INPUT = "wrongInput";
    public static String APP = "app";
    public static String TYPE = "type";
    public static String CHAT = "chat";
    public static String FROM = "from";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String ENTITIES = "serviceentities";
    public static String NAMES = "names";

    public static String TRACKING_ID = "correlationId";

    public static String FRIEND="friend";
    public static final String USER_FIRST_NAME="first_name";

    public static final String NAIROBI="Africa/Nairobi";

    public static final String BOT_INFO="appInfo";
    public static final String USERDETAILS="userDetails";
    public static final String USERINFO="userInfo";


    public static String REQTYPE_INCOMING = "incoming";

    public static String CORRELATION_ID = "correlationId";

    public static String RESPONSE_CODE="response_code";
    public static String DESCRIPTION="description";

    public static final String FACEBOOK = "FACEBOOK";
    public static final String TELEGRAM = "TELEGRAM";
    public static final String WEBCHAT = "WEBCHAT";
    public static String WEBCHAT_USER = "userId";
    public static String WEBCHAT_TEXT = "text";
    public static String SECURE = "secure";

    public static String REQUEST_FOR_CHAT_FLOW = "Recieved chat from social media";
    public static String REQUEST_FOR_CHAT_FLOW_CALLBACK = "Test Callback";
    public static String REQUEST_FOR_CHAT_FLOW_401 = "Recieved chat from social media but unauthorized";
    public static String REQUEST_FOR_IMAGES = "Recieved request for images";

    public static String AFRICA_NAIROBI = "Africa/Nairobi";
}
