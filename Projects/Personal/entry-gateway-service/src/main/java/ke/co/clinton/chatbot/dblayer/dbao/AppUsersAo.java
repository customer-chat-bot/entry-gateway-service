package ke.co.clinton.chatbot.dblayer.dbao;

import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.dblayer.repos.AppUserRepo;
import ke.co.clinton.chatbot.dblayer.repos.AppsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AppUsersAo implements  RepoInterface {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    AppUserRepo appUserRepo;

    @Autowired
    AppsRepo appsRepo;

    @Override
    public Object add(Object object) {
        AppUser appUser = (AppUser) object;
        return appUserRepo.save(appUser);
    }

    @Override
    public Object update(Object object) {
        return add(object);
    }

    public void removeUser(AppUser appUser){
        appUserRepo.delete(appUser);
    }

    public AppUser getOneChatUser(String appName, String appId, String userAppId){
        Optional<AppUser> appUserOptional = appUserRepo.findUserByNameAndSecretID(appName, appId, userAppId);
        return appUserOptional.orElse(null);
    }

    public Apps getAppForAll(String appName, String appSecretId){
        Optional<Apps> appsOptional = appsRepo.findAppByNameAndSecretID(appName, appSecretId);
        return appsOptional.orElse(null);
    }

    public Apps addApp(Object object) {
        Apps apps = (Apps) object;
        return appsRepo.save(apps);
    }

    public Apps getAppByToken(String authToken) {
        Optional<Apps> appsOptional = appsRepo.findAppByAuthToken(authToken);
        return appsOptional.orElse(null);
    }
    public void removeApp(Apps apps){
        appsRepo.delete(apps);
    }


}
