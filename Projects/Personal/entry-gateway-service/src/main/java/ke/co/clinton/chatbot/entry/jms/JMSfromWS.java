package ke.co.clinton.chatbot.entry.jms;

import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.services.datainit.WsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JMSfromWS {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(JMSfromWS.class);

    @Autowired
    WsService wsService;

    @Autowired
    Config config;

    @RabbitListener(queues = "${ws-send-message-queue}")
    public void getMessageForSessionEnrichment(String message) {
        String trackingId = config.getTrackingId();
        long startTime = config.getTimeNow();
        try{
            wsService.sendMessage(startTime, trackingId, message);
        }
        catch(Exception e){
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500,GlobalVariables.REQUEST_FOR_CHAT_FLOW,logs.errorMessage(e), startTime));
        }
    }
}
