package ke.co.clinton.chatbot.dblayer.dataholders;

import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import org.json.JSONObject;

public class UserDetails {

    private AppUser userInfo;
    private Apps botInfo;


    public UserDetails(Apps apps, AppUser appUser, boolean firstTimeUser) {
        this.userInfo = appUser;
        this.userInfo.setFirstTime(firstTimeUser);
        this.botInfo = apps;
    }

    public AppUser getUserInfo() {
        return userInfo;
    }

    public Apps getAppsInfo() {
        return botInfo;
    }

    @Override
    public String toString() {
        JSONObject response = new JSONObject();
        response.put(GlobalVariables.USERDETAILS, new JSONObject()
                .put(GlobalVariables.BOT_INFO, getAppsInfo().asJson())
                .put(GlobalVariables.USERINFO, new JSONObject(getUserInfo().toString())));
        return response.toString();
    }
}
