/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.entry.controllers;

import ke.co.clinton.chatbot.config.SharedData;
import ke.co.clinton.chatbot.services.datainit.TelegramInitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/app/telegram",produces="application/json")
public class TelegramController {

    @Autowired
    private TelegramInitService telegramInitService;

    @Autowired
    SharedData sharedData;

    @PostMapping
    public ResponseEntity<String> getBotbydetails(@RequestBody String req) {
        return  sharedData.marshalResponse(telegramInitService.queueRequestRecieved("6", req));
    }

    @PostMapping(value="/{id}")
    public ResponseEntity<String> getBotbydetailsMultiple(@PathVariable String id,@RequestBody String req) {
        return  sharedData.marshalResponse(telegramInitService.queueRequestRecieved(id, req));
    }
    
}

