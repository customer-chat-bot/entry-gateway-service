package ke.co.clinton.chatbot.dblayer.dbao;

public interface RepoInterface {

    Object add(Object object);
    Object update(Object object);
}
