package ke.co.clinton.chatbot.dblayer.repos;


import ke.co.clinton.chatbot.dblayer.entities.Apps;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppsRepo extends MongoRepository<Apps, String> {

    @Query("{$and:[ {'appName': ?0},{'appSecretId': ?1}]}")
    Optional<Apps> findAppByNameAndSecretID(String appName, String appSecretId);

    @Query("{$and:[ {'authToken': ?0}] }")
    Optional<Apps> findAppByAuthToken(String authToken);

}
