/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.clinton.chatbot.services.datainit;

import ke.co.clinton.chatbot.config.Config;
import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.entry.requests.BotResponse;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.services.datalogic.UserMessageProcessor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TelegramInitService {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(TelegramInitService.class);
    @Autowired
    SharedResource sharedResource;

    @Autowired
    Config config;

    @Autowired
    UserMessageProcessor userMessageProcessor;

    public BotResponse queueRequestRecieved(String id, String data){
        JSONObject response = null;
        int code = 0;
        String trackingId = config.getTrackingId();
        long startTime = config.getTimeNow();
        try{
            JSONObject info = new JSONObject(data);
            info.put(GlobalVariables.ID, id);
            response = sharedResource.queueRequest(startTime,  trackingId, GlobalVariables.TELEGRAM , info.toString());
            code = 200;
        }
        catch(Exception e){
            code = 500;
            logger.error(logs.addlogger(trackingId, GlobalVariables.ERROR_CODE_500, data, logs.errorMessage(e), startTime));
        }
        return new BotResponse(code, response);
    }

    public BotUserMessage processAfterQueueing(long startTime, String trackingId, String botId,String input){
        BotUserMessage botUserMessage = userMessageProcessor.getMessageFromTelegram(startTime, trackingId, botId, input);
        return sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
    }
    
    
    
    
    
}
