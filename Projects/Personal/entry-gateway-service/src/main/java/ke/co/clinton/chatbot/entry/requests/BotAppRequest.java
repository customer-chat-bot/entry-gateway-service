package ke.co.clinton.chatbot.entry.requests;

import javax.validation.constraints.NotNull;

public class BotAppRequest {
    @NotNull(message = "appName is required" )
    private String appName;
    @NotNull(message = "secretId is required" )
    private String secretId;
    @NotNull(message = "webhook is required" )
    private String webhook;

    public BotAppRequest() {
    }

    public BotAppRequest(@NotNull(message = "appName is required") String appName, @NotNull(message = "secretId is required") String secretId, @NotNull(message = "webhook is required") String webhook) {
        this.appName = appName;
        this.secretId = secretId;
        this.webhook = webhook;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getWebhook() {
        return webhook;
    }

    public void setWebhook(String webhook) {
        this.webhook = webhook;
    }

    @Override
    public String toString() {
        return "BotAppRequest{" +
                "appName='" + appName + '\'' +
                ", secretId='" + secretId + '\'' +
                ", webhook='" + webhook + '\'' +
                '}';
    }
}
