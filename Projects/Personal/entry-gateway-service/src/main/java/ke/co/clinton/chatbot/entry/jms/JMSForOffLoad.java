package ke.co.clinton.chatbot.entry.jms;

import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.config.LogsManager;
import ke.co.clinton.chatbot.entry.requests.BotOnlineRequest;
import ke.co.clinton.chatbot.services.datainit.WsService;
import ke.co.clinton.chatbot.services.datainit.OffloaderProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JMSForOffLoad {

    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(JMSForOffLoad.class);

    @Autowired
    WsService wsService;

    @Autowired
    OffloaderProcessor offloaderProcessor;

    @RabbitListener(queues = "${requests-queue-offloader}")
    public void getMessageForSessionEnrichment(BotOnlineRequest message) {
        try{
            offloaderProcessor.processRequestFromAPP(message);
        }
        catch(Exception e){
            logger.error(logs.addlogger(message.getTrackingId(), GlobalVariables.ERROR_CODE_500, GlobalVariables.REQUEST_FOR_CHAT_FLOW, logs.errorMessage(e), message.getStartTime()));
        }
    }
}
