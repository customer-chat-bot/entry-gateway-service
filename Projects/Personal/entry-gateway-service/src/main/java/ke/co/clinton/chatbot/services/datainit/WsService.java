package ke.co.clinton.chatbot.services.datainit;

import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import ke.co.clinton.chatbot.services.datalogic.UserMessageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WsService {

    @Autowired
    SharedResource sharedResource;

    @Autowired
    UserMessageProcessor userMessageProcessor;


    public BotUserMessage sendMessage(long startTime, String trackingId, String data){
        BotUserMessage botUserMessage = userMessageProcessor.getMessageFromWebSockets(startTime, trackingId, data);
        return sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
    }

}
