package ke.co.clinton.chatbot.entry.requests;

import javax.validation.constraints.NotNull;

public class OtherAppMessageRequest {

    @NotNull(message = "userId is required" )
    private String userId;
    @NotNull(message = "message is required" )
    private String message;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
