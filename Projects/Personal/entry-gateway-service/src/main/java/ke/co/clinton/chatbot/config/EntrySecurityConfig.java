package ke.co.clinton.chatbot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class EntrySecurityConfig extends WebSecurityConfigurerAdapter {

    // Authentication : User --> Roles
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance())
                .withUser("botadmin").password("231c3adc87-d53b-43d8-acfa-3ae5d7b1a43")
                .roles("USER", "ADMIN");
    }
    // Authorization : Role -> Access
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and()
                .authorizeRequests()
                .antMatchers("/","/info","swagger-ui.html","/v2/api-docs").permitAll()
                .anyRequest().hasRole("ADMIN")
                .and()
                .csrf().disable().headers().frameOptions().disable();
    }
}
