package ke.co.clinton.chatbot;

public class Props {

    public String getFaceBookTextRequest(){
        return "{\n" +
                "  \"object\": \"page\",\n" +
                "  \"entry\": [\n" +
                "    {\n" +
                "      \"messaging\": [\n" +
                "        {\n" +
                "          \"message\": {\n" +
                "            \"text\": \"test\",\n" +
                "            \"seq\": 20,\n" +
                "            \"mid\": \"mid.1466015\"\n" +
                "          },\n" +
                "          \"timestamp\": 1466015596919,\n" +
                "          \"sender\": {\n" +
                "            \"id\": \"1735639539807230\"\n" +
                "          },\n" +
                "          \"recipient\": {\n" +
                "            \"id\": \"898786793658413\"\n" +
                "          }\n" +
                "        }\n" +
                "      ],\n" +
                "      \"time\": 1466015596947,\n" +
                "      \"id\": \"898786793658413\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }

    public String getFaceBookQuickReplies(){
        return "{\n" +
                "    \"object\": \"page\",\n" +
                "    \"entry\": [\n" +
                "        {\n" +
                "            \"time\": 1466015596947,\n" +
                "            \"id\": \"898786793658413\",\n" +
                "            \"messaging\": [\n" +
                "                {\n" +
                "                    \"sender\": {\n" +
                "                        \"id\": \"1735639539807230\"\n" +
                "                    },\n" +
                "                    \"recipient\": {\n" +
                "                        \"id\": \"898786793658413\"\n" +
                "                    },\n" +
                "                    \"timestamp\": 1502905976377,\n" +
                "                    \"message\": {\n" +
                "                        \"quick_reply\": {\n" +
                "                            \"payload\": \"test\"\n" +
                "                        },\n" +
                "                        \"mid\": \"m_AG5Hz2Uq7tuwNEhXfYYKj8mJEM_QPpz5jdCK48PnKAjSdjfipqxqMvK8ma6AC8fplwlqLP_5cgXIbu7I3rBN0P\",\n" +
                "                        \"text\": \"Green\"\n" +
                "                    }\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";
    }

    public String getFaceBookButtonPayLoads(){
        return "";
    }


    public String getWebSockets(){
        return "{\n" +
                "    \"name\": \"zuri\",\n" +
                "    \"phoneNumber\": \"0726617530\",\n" +
                "    \"userId\": \"578989373983\",\n" +
                "    \"text\": \"test\"\n" +
                "}";
    }

    public String getTelegramTextRequest(){
        return "{\n" +
                "    \"update_id\":646911460,\n" +
                "    \"message\":{\n" +
                "        \"message_id\":93,\n" +
                "        \"from\":{\n" +
                "            \"id\":1378900389,\n" +
                "            \"is_bot\":false,\n" +
                "            \"first_name\":\"Clinton\",\n" +
                "            \"username\":\"Wekesa\",\n" +
                "            \"language_code\":\"en-US\"\n" +
                "        },\n" +
                "        \"chat\":{\n" +
                "            \"id\":1378900389,\n" +
                "            \"first_name\":\"Clinton\",\n" +
                "            \"username\":\"Wekesa\",\n" +
                "            \"type\":\"private\"\n" +
                "        },\n" +
                "        \"date\":1509641174,\n" +
                "        \"text\":\"test\"\n" +
                "    }\n" +
                "}";
    }
}
