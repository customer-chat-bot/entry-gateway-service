package ke.co.clinton.chatbot;


import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.entry.requests.*;
import ke.co.clinton.chatbot.services.datainit.*;
import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.dblayer.dbao.AppUsersAo;
import ke.co.clinton.chatbot.entry.requests.*;
import ke.co.clinton.chatbot.services.datainit.*;
import ke.co.clinton.chatbot.services.datalogic.UserMessageProcessor;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@DisplayName("Requests from FaceBook & Telegram")
public class RequestsProcessorTests {

    @Autowired
    UserMessageProcessor userMessageProcessor;

    @Autowired
    FacebookinitService facebookinitService;
    @Autowired
    TelegramInitService telegramInitService;
    @Autowired
    WsService wsService;
    @Autowired
    OffloaderProcessor offloaderProcessor;

    @Autowired
    SharedResource sharedResource;
    @Autowired
    AppUsersAo appUsersAo;

    @Autowired
    OtherAppsService otherAppsService;

    String trackingId = "testId";
    long startTime = 0;

    String telegramId = "testId";

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    @DisplayName("Facebook Requests")
    class FaceBook{
        BotUserMessage botUserMessage;

        @Test
        public void validateFacebookInit() {
            Assertions.assertEquals("hello", facebookinitService.processInit("safaricom_web_test_bot", "hello"));
        }

        @Test
        public void validateFacebookInitNotSame() {
            Assertions.assertNotEquals("hell", facebookinitService.processInit("safaricom_web_test_bo", "hello"), "Should not be same");
        }

        @Test
        public void validateFacebookInitNotNullwhereNoToken() {
            Assertions.assertNotNull(facebookinitService.processInit(null, "hello"));
        }

        @Test
        public void checkIfObjectNotNull() {
            String customerRequest = new Props().getFaceBookTextRequest();
            botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
            Assertions.assertNotNull(botUserMessage, "User Object can not be null");
        }

        @Test
        public void checkIfObjectNull() {
            botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, null);
            Assertions.assertNull(botUserMessage, "User Object should be null as theres No data");
        }

        @Test
        public void setBotUserMessageObject() {
            String customerRequest = new Props().getFaceBookTextRequest();
            botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.FACEBOOK, botUserMessage.getBotName(), "The App Name should be Facebook")
            );
        }

        @Test
        public void getUserQuickRepliesButtonClick() {
            String customerRequest = new Props().getFaceBookQuickReplies();
            botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
            Assertions.assertNotNull(botUserMessage.getMessage(), "Quick replies has to be extracted");
        }

        @Test
        public void userFromFaceBookInputTest() {
            String customerRequest = new Props().getFaceBookTextRequest();
            botUserMessage = facebookinitService.processAfterQueueing(startTime, trackingId, customerRequest);
            BotResponse response2 = facebookinitService.queueRequestRecieved(customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.FACEBOOK, botUserMessage.getBotName(), "The App Name should be Facebook"),
                    () -> Assertions.assertEquals("test", botUserMessage.getMessage(), "The message should be test"),
                    () -> Assertions.assertEquals(200, response2.getResponseCode(), "Should be 200 cause its added")
            );
        }

        @AfterEach
        public void removeUser(){
            System.out.println("Running remove User");
            String customerRequest = new Props().getFaceBookTextRequest();
            BotUserMessage botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
            botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
            AppUser appUser = botUserMessage.getUserDetails().getUserInfo();
            appUsersAo.removeUser(appUser);
            System.out.println("Done");
        }

    }

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    @DisplayName("Telegram Requests")
    class Telegram{


        BotUserMessage botUserMessage;
        @BeforeAll
        public void tearDownWholeAppBefore(){
            tearDownWholeApp();
        }
        @AfterAll
        public void tearDownWholeAppAfter(){
            tearDownWholeApp();
        }
        public void tearDownWholeApp() {
            String customerRequest = new Props().getTelegramTextRequest();
            botUserMessage = telegramInitService.processAfterQueueing(startTime, trackingId, telegramId, customerRequest);
            Apps apps = botUserMessage.getUserDetails().getAppsInfo();
            if(apps != null){
                System.out.println("removing app");
                appUsersAo.removeApp(apps);
            }

            AppUser appUser = botUserMessage.getUserDetails().getUserInfo();
            if(appUser != null){
                appUsersAo.removeUser(appUser);
            }
        }



        @Test
        public void checkIfObjectNotNull() {
            String customerRequest = new Props().getTelegramTextRequest();
            botUserMessage = userMessageProcessor.getMessageFromTelegram(startTime, trackingId, telegramId, customerRequest);
            Assertions.assertNotNull(botUserMessage, "User Object can not be null");
        }

        @Test
        public void checkIfObjectNull() {
            botUserMessage = userMessageProcessor.getMessageFromTelegram(startTime, trackingId, telegramId, null);
            Assertions.assertNull(botUserMessage, "User Object should be null as theres No data");
        }

        @Test
        public void setBotUserMessageObject() {
            String customerRequest = new Props().getTelegramTextRequest();
            botUserMessage = userMessageProcessor.getMessageFromTelegram(startTime, trackingId, telegramId, customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Telegram is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.TELEGRAM, botUserMessage.getBotName(), "The App Name should be Telegram")
            );
        }

        @Test
        public void getUserQuickRepliesButtonClick() {
            String customerRequest = new Props().getFaceBookQuickReplies();
            botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
            Assertions.assertNotNull(botUserMessage.getMessage(), "Quick replies has to be extracted");
        }

        @Test
        public void userFromTelegramInputTest() {
            String customerRequest = new Props().getTelegramTextRequest();
            botUserMessage = telegramInitService.processAfterQueueing(startTime, trackingId, telegramId, customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Telegram is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.TELEGRAM, botUserMessage.getBotName(), "The App Name should be Telegram"),
                    () -> Assertions.assertEquals("test", botUserMessage.getMessage(), "The message should be test")
            );
        }

        @Test
        public void userFromTelegramInputTestNull() {
            botUserMessage = telegramInitService.processAfterQueueing(startTime, trackingId, telegramId, null);
            Assertions.assertNull(botUserMessage, "Should be Null");
        }

        @Test
        public void userFromTelegramInputTestForQueueing() {
            String customerRequest = new Props().getTelegramTextRequest();
            BotResponse response = telegramInitService.queueRequestRecieved(trackingId, customerRequest);
            BotResponse response2 = telegramInitService.queueRequestRecieved(trackingId, null);
            Assertions.assertAll(
                    ()->Assertions.assertEquals(200, response.getResponseCode(), "Should be same"),
                    () -> Assertions.assertEquals(500, response2.getResponseCode(), "Should be 500 cause its null")

            );
        }

    }


    @Nested
    @DisplayName("Websockets tests")
    class Websockets{

        BotUserMessage botUserMessage;

        @Test
        public void checkIfObjectNotNull() {
            String customerRequest = new Props().getWebSockets();
            botUserMessage = userMessageProcessor.getMessageFromWebSockets(startTime, trackingId, customerRequest);
            Assertions.assertNotNull(botUserMessage, "User Object can not be null");
        }

        @Test
        public void checkIfObjectNull() {
            botUserMessage = userMessageProcessor.getMessageFromWebSockets(startTime, trackingId, null);
            Assertions.assertNull(botUserMessage, "User Object should be null as theres No data");
        }

        @Test
        public void setBotUserMessageObject() {
            String customerRequest = new Props().getWebSockets();
            botUserMessage = userMessageProcessor.getMessageFromWebSockets(startTime, trackingId, customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.WEBCHAT, botUserMessage.getBotName(), "The App Name should be WebChat"),
                    () -> Assertions.assertEquals("test", botUserMessage.getMessage(), "User text should be test")
            );
        }

        @Test
        public void userFromWSInputTest() {
            String customerRequest = new Props().getWebSockets();
            botUserMessage = wsService.sendMessage(startTime, trackingId, customerRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.WEBCHAT, botUserMessage.getBotName(), "The App Name should be Webchat"),
                    () -> Assertions.assertEquals("test", botUserMessage.getMessage(), "The message should be test")
            );
        }

        @Test
        public void userFromWSInputTestForNull() {
            botUserMessage = wsService.sendMessage(startTime, trackingId, null);
            Assertions.assertNull(botUserMessage, "Should be null");
        }


    }


    @Nested
    @DisplayName("General User tests")
    class GeneralUser{

        BotUserMessage botUserMessage;

        @Test
        public void setBotUserMessageObjectFromUnkownApp() {
            BotOnlineRequest botOnlineRequest = new BotOnlineRequest(trackingId, "Test", null,startTime);
            botUserMessage = offloaderProcessor.processRequestFromAPP(botOnlineRequest);
            Assertions.assertNull(botUserMessage, "Should be null coz it has not been updated");
        }

        @Test
        public void setBotUserMessageObjectFromTelegramNull() {
            BotOnlineRequest botOnlineRequest = new BotOnlineRequest(trackingId, GlobalVariables.TELEGRAM, null,startTime);
            botUserMessage = offloaderProcessor.processRequestFromAPP(botOnlineRequest);
            Assertions.assertNull(botUserMessage, "Should be null coz it has not been updated");
        }

        @Test
        public void setBotUserMessageObjectFromFaceBook() {
            String customerRequest = new Props().getFaceBookTextRequest();

            BotOnlineRequest botOnlineRequest = new BotOnlineRequest(trackingId, GlobalVariables.FACEBOOK, customerRequest,startTime);
            botUserMessage = offloaderProcessor.processRequestFromAPP(botOnlineRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.FACEBOOK, botUserMessage.getBotName(), "The App Name should be Facebook"),
                    () -> Assertions.assertNotNull( botUserMessage.getUserDetails(), "The App User cant not be null")
            );
        }

        @Test
        public void setBotUserMessageObjectFromTelegram() throws JSONException {
            String customerRequest = new Props().getTelegramTextRequest();
            JSONObject info = new JSONObject(customerRequest);
            info.put(GlobalVariables.ID, telegramId);
            BotOnlineRequest botOnlineRequest = new BotOnlineRequest(trackingId, GlobalVariables.TELEGRAM, info.toString(),startTime);
            botUserMessage = offloaderProcessor.processRequestFromAPP(botOnlineRequest);
            Assertions.assertAll(
                    () -> Assertions.assertFalse(botUserMessage.isSecure(), "Telegram is not a secure channel"),
                    () -> Assertions.assertEquals(GlobalVariables.TELEGRAM, botUserMessage.getBotName(), "The App Name should be Facebook"),
                    () -> Assertions.assertNotNull( botUserMessage.getUserDetails(), "The App User cant not be null")
            );
        }

        @Test
        public void whenObjectIsNull() {
            botUserMessage = offloaderProcessor.processRequestFromAPP(null);
            Assertions.assertNull( botUserMessage, "The App User should be null");
        }



    }

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    @DisplayName("Third party apps")
    class ThirdParty{

       String appToken;

        @BeforeAll
        public void addApp() throws JSONException {
            System.out.println("Now we are creating third party app");
            BotResponse botResponse = otherAppsService.addApp(new BotAppRequest("junit", "tester", "zuri.com"));
            JSONObject response = botResponse.getResponse();
            appToken = response.has("authToken") ? response.getString("authToken") : null;
        }

        @Test
        public void sendMessage(){
            OtherAppMessageRequest otherAppMessageRequest =  new OtherAppMessageRequest();
            otherAppMessageRequest.setMessage("test");
            otherAppMessageRequest.setUserId("1");
            BotResponse botResponse = otherAppsService.sendMessage(appToken, otherAppMessageRequest);
            Assertions.assertEquals(botResponse.getResponseCode(), GlobalVariables.SUCCESS_CODE_200);
        }

        @Test
        public void sendMessageForUnauthorized(){
            OtherAppMessageRequest otherAppMessageRequest =  new OtherAppMessageRequest();
            otherAppMessageRequest.setMessage("test");
            otherAppMessageRequest.setUserId("1");
            BotResponse botResponse = otherAppsService.sendMessage("touiss", otherAppMessageRequest);
            Assertions.assertEquals(botResponse.getResponseCode(), GlobalVariables.ERROR_CODE_401);
        }

        @Test
        public void sendMessageForNullInput(){
            BotResponse botResponse = otherAppsService.sendMessage(appToken, null);
            Assertions.assertEquals(botResponse.getResponseCode(), GlobalVariables.ERROR_CODE_500, "Passed null values");
        }

        @AfterAll
        public void tearDownWholeApp() {
            System.out.println("Token : "+ appToken);
            Apps apps = appUsersAo.getAppByToken(appToken);
            if(apps != null){
                System.out.println("removing app");
                appUsersAo.removeApp(apps);
            }
        }

    }


}
