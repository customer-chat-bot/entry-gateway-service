package ke.co.clinton.chatbot;


import ke.co.clinton.chatbot.config.GlobalVariables;
import ke.co.clinton.chatbot.dblayer.entities.AppUser;
import ke.co.clinton.chatbot.dblayer.entities.Apps;
import ke.co.clinton.chatbot.entry.requests.BotUserMessage;
import ke.co.clinton.chatbot.services.datalogic.SharedResource;
import ke.co.clinton.chatbot.dblayer.dbao.AppUsersAo;
import ke.co.clinton.chatbot.services.datalogic.UserMessageProcessor;
import ke.co.clinton.chatbot.services.datalogic.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DisplayName("Platform User tests")
public class UserAccountProcessorTests {


    @Autowired
    SharedResource sharedResource;
    @Autowired
    UserMessageProcessor userMessageProcessor;

    @Autowired
    UserService userService;
    @Autowired
    AppUsersAo appUsersAo;

    String trackingId = "testId";
    long startTime = 0;

    BotUserMessage botUserMessage;

    @BeforeEach
    public void init(){
        String customerRequest = new Props().getWebSockets();
        botUserMessage = userMessageProcessor.getMessageFromWebSockets(startTime, trackingId, customerRequest);
    }

    @Test
    public void getUserDetailsObjectForWebSocket(){
        botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
        //here we are running tests for the websockets now
        Assertions.assertAll(
                () -> Assertions.assertFalse(botUserMessage.isSecure(), "Web is not a secure channel"),
                () -> Assertions.assertEquals(GlobalVariables.WEBCHAT, botUserMessage.getBotName(), "The App Name should be WebChat"),
                () -> Assertions.assertEquals("test", botUserMessage.getMessage(), "User text should be test"),
                () -> Assertions.assertEquals("zuri", botUserMessage.getUserDetails().getUserInfo().getNames(), "The names should be same")
        );
    }

    @Test
    public void getUserDetailsObjectForFacebook() {
        String customerRequest = new Props().getFaceBookTextRequest();
        botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);

        botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
        //here we are running tests for the websockets now
        Assertions.assertAll(
                () -> Assertions.assertFalse(botUserMessage.isSecure(), "Facebook is not a secure channel"),
                () -> Assertions.assertEquals(GlobalVariables.FACEBOOK, botUserMessage.getBotName(), "The App Name should be FB")
        );
    }

    @Test
    public void getUserDetailsForNull(){
        botUserMessage = null;
        botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
        //here we are running tests for the websockets now
        Assertions.assertNull(botUserMessage, "It should be null");
    }

    @RepeatedTest(2)
    public void getUserDetailsTestAddingNumbersToAccount(){
        String customerRequest = new Props().getFaceBookTextRequest();
        botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
        botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
        AppUser appUser = botUserMessage.getUserDetails().getUserInfo();
        appUser = userService.addPhoneNumber(startTime, trackingId, "0722000000", appUser);
        //here we are running tests for the websockets now
        Assertions.assertTrue(appUser.getAccounts().size() > 0, "The accounts need to be more than 0 now");
    }

    @Test
    public void getUserName() {
        String customerRequest = new Props().getFaceBookTextRequest();
        BotUserMessage botUserMessage = userMessageProcessor.getMessageFromFacebook(startTime, trackingId, customerRequest);
        botUserMessage = sharedResource.createBotUserInfo(startTime, trackingId, botUserMessage);
        AppUser appUser = botUserMessage.getUserDetails().getUserInfo();
        Apps apps = botUserMessage.getUserDetails().getAppsInfo();

        String names = userService.getUsername(startTime, trackingId, apps, appUser);
        Assertions.assertNotEquals(names, GlobalVariables.FRIEND, "We need to pull user details");
    }

    @Test
    public void getUserDetailsTestAddingNumbersToAccountForNullUser(){

        AppUser appUser = userService.addPhoneNumber(startTime, trackingId, "0722000000", null);
        //here we are running tests for the websockets now
        Assertions.assertNull(appUser, "The appUser should be null");
    }

}
